[DFHRF] Matthias Jakob, Oldrich Hungr. Debris Flows Hazars and Related Phenomena. 1a ed. Springer.  2005 Alemania

[Volcanoes]

Vallance, J.W. (2000). Lahars. En Encyclopedia of Volcanoes. San Diego: Academic Press.


[34]

Akyildiz, I. F., Su, W., Sankarasubramaniam, Y., & Cayirci, E. (2002). Wireless sensor networks: a survey. Computer Networks, 38(4), 393-422. Elsevier. Retrieved from http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024

@article{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002, title={Wireless sensor networks: a survey}, volume={38}, url={http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024}, number={4}, journal={Computer Networks}, publisher={Elsevier}, author={Akyildiz, I F and Su, W and Sankarasubramaniam, Y and Cayirci, E}, year={2002}, pages={393--422}}



[51]

Sameer Tilak, Nael B. Abu-Ghazaleh, and Wendi Heinzelman. 2002. A taxonomy of wireless micro-sensor network models. SIGMOBILE Mob. Comput. Commun. Rev. 6, 2 (April 2002), 28-36. DOI=10.1145/565702.565708 http://doi.acm.org/10.1145/565702.565708 

@article{Tilak:2002:TWM:565702.565708,
 author = {Tilak, Sameer and Abu-Ghazaleh, Nael B. and Heinzelman, Wendi},
 title = {A taxonomy of wireless micro-sensor network models},
 journal = {SIGMOBILE Mob. Comput. Commun. Rev.},
 issue_date = {April 2002},
 volume = {6},
 number = {2},
 month = apr,
 year = {2002},
 issn = {1559-1662},
 pages = {28--36},
 numpages = {9},
 url = {http://doi.acm.org/10.1145/565702.565708},
 doi = {10.1145/565702.565708},
 acmid = {565708},
 publisher = {ACM},
 address = {New York, NY, USA},
}



[187]

Akyildiz, I. F., & Vuran, M. C. (2002). Draft – ( DO NOT DISTRIBUTE ) Wireless Sensor Networks : A Survey Revisited. Computer Networks, 38(4), 1-45. Ieee. Retrieved from http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024

@article{Akyildiz_Vuran_2002, title={Draft – ( DO NOT DISTRIBUTE ) Wireless Sensor Networks : A Survey Revisited}, volume={38}, url={http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024}, number={4}, journal={Computer Networks}, publisher={Ieee}, author={Ak
yildiz, Ian F and Vuran, Mehmet C}, year={2002}, pages={1--45}}


[20]
Hart, J. K., & Martinez, K. (2006). Environmental Sensor Networks: a revolution in the earth system science? Earth-Science Reviews, 78(3-4), 177-191. Elsevier. Retrieved from http://dx.doi.org/10.1016/j.earscirev.2006.05.001

@article{Hart_Martinez_2006, title={Environmental Sensor Networks: a revolution in the earth system science?}, volume={78}, url={http://dx.doi.org/10.1016/j.earscirev.2006.05.001}, number={3-4}, journal={Earth-Science Reviews}, publisher={Elsevier}, author={Hart, Jane K and Martinez, Kirk}, year={2006}, pages={177--191}}


[58]

Chong, C.-Y. C. C.-Y., & Kumar, S. P. (2003). Sensor networks: evolution, opportunities, and challenges. Proceedings of the IEEE. IEEE. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1219475

@misc{Chong_Kumar_2003, title={Sensor networks: evolution, opportunities, and challenges}, volume={91}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1219475}, number={8}, journal={Proceedings of the IEEE}, publisher={IEEE}, author={Chong, Chee-Yee Chong Chee-Yee and Kumar, S P}, year={2003}, pages={1247--1256}}


[0A]
Arattano, M., & Marchi, L. (2008). Systems and Sensors for Debris-flow Monitoring and Warning. Sensors (Peterboroug, 8(4), 2436-2452. Retrieved from http://www.mdpi.com/1424-8220/8/4/2436/

@article{Arattano_Marchi_2008, title={Systems and Sensors for Debris-flow Monitoring and Warning}, volume={8}, url={http://www.mdpi.com/1424-8220/8/4/2436/}, number={4}, journal={Sensors (Peterboroug}, author={Arattano, Massimo and Marchi, Lorenzo}, year={2008}, pages={2436--2452}}









