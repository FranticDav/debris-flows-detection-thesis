
[280]

Kotz, D., Newport, C., & Elliott, C. (2003). The mistaken axioms of wireless-network research. Dartmouth College Computer Science Technical Report, TR2003-467(TR2003-467), 1-14. Retrieved from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.147.889&rep=rep1&type=pdf

@article{Kotz_Newport_Elliott_2003, title={The mistaken axioms of wireless-network research}, volume={TR2003-467}, url={http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.147.889&rep=rep1&type=pdf}, number={TR2003-467}, journal={Dartmouth College Computer Science Technical Report}, author={Kotz, David and Newport, Calvin and Elliott, Chip}, year={2003}, pages={1--14}}


[278]
Zhou, G., He, T., Krishnamurthy, S., & Stankovic, J. A. (2004). Impact of radio irregularity on wireless sensor networks. Proceedings of the 2nd international conference on Mobile systems applications and services MobiSYS 04, 125. ACM Press. Retrieved from http://portal.acm.org/citation.cfm?doid=990064.990081

@article{Zhou_He_Krishnamurthy_Stankovic_2004, title={Impact of radio irregularity on wireless sensor networks}, url={http://portal.acm.org/citation.cfm?doid=990064.990081}, journal={Proceedings of the 2nd international conference on Mobile systems applications and services MobiSYS 04}, publisher={ACM Press}, author={Zhou, Gang and He, Tian and Krishnamurthy, Sudha and Stankovic, John A}, year={2004}, pages={125}}


[269]
Wang, X., Xing, G., Zhang, Y., Lu, C., Pless, R., & Gill, C. (2003). Integrated coverage and connectivity configuration in wireless sensor networks. Proceedings of the first international conference on Embedded networked sensor systems SenSys 03, pp, 28. ACM Press. Retrieved from http://portal.acm.org/citation.cfm?doid=958491.958496

@article{Wang_Xing_Zhang_Lu_Pless_Gill_2003, title={Integrated coverage and connectivity configuration in wireless sensor networks}, volume={pp}, url={http://portal.acm.org/citation.cfm?doid=958491.958496}, journal={Proceedings of the first international conference on Embedded networked sensor systems SenSys 03}, publisher={ACM Press}, author={Wang, Xiaorui and Xing, Guoliang and Zhang, Yuanfang and Lu, Chenyang and Pless, Robert and Gill, Christopher}, year={2003}, pages={28}}




[276]
Zhang, H., & Hou, J. C. (2005). Maintaining Sensing Coverage and Connectivity in Large Sensor Networks. Science, 1(1-2), 89-124. Citeseer. Retrieved from http://www.oldcitypublishing.com/AHSWN/AHSWN

@article{Zhang_Hou_2005, title={Maintaining Sensing Coverage and Connectivity in Large Sensor Networks}, volume={1}, url={http://www.oldcitypublishing.com/AHSWN/AHSWN}, number={1-2}, journal={Science}, publisher={Citeseer}, author={Zhang, Honghai and Hou, Jennifer C}, year={2005}, pages={89--124}}



[270]

Younis, M., & Akkaya, K. (2008). Strategies and techniques for node placement in wireless sensor networks: A survey. Ad Hoc Networks, 6(4), 621-655. Elsevier Science Publishers B. V. Retrieved from http://linkinghub.elsevier.com/retrieve/pii/S1570870507000984

@article{Younis_Akkaya_2008, title={Strategies and techniques for node placement in wireless sensor networks: A survey}, volume={6}, url={http://linkinghub.elsevier.com/retrieve/pii/S1570870507000984}, number={4}, journal={Ad Hoc Networks}, publisher={Elsevier Science Publishers B. V.}, author={Younis, M and Akkaya, K}, year={2008}, pages={621--655}}


[277]
Tang, J., Hao, B., & Sen, A. (2006). Relay node placement in large scale wireless sensor networks. Computer Communications, 29(4), 490-501. Elsevier. Retrieved from http://linkinghub.elsevier.com/retrieve/pii/S0140366405000459


@article{Tang_Hao_Sen_2006, title={Relay node placement in large scale wireless sensor networks}, volume={29}, url={http://linkinghub.elsevier.com/retrieve/pii/S0140366405000459}, number={4}, journal={Computer Communications}, publisher={Elsevier}, author={Tang, J and Hao, B and Sen, A}, year={2006}, pages={490--501}}



[271]

Jason Hill, Mike Horton, Ralph Kling, and Lakshman Krishnamurthy. 2004. The platforms enabling wireless sensor networks. Commun. ACM 47, 6 (June 2004), 41-46. DOI=10.1145/990680.990705 http://doi.acm.org/10.1145/990680.990705 

@article{Hill:2004:PEW:990680.990705,
 author = {Hill, Jason and Horton, Mike and Kling, Ralph and Krishnamurthy, Lakshman},
 title = {The platforms enabling wireless sensor networks},
 journal = {Commun. ACM},
 issue_date = {June 2004},
 volume = {47},
 number = {6},
 month = jun,
 year = {2004},
 issn = {0001-0782},
 pages = {41--46},
 numpages = {6},
 url = {http://doi.acm.org/10.1145/990680.990705},
 doi = {10.1145/990680.990705},
 acmid = {990705},
 publisher = {ACM},
 address = {New York, NY, USA},
} 



[279]
Ruiz-Sandoval, M., Tomonori, N., & Spencer, B. F. (2006). Sensor development using Berkeley mote platform. Journal of Earthquake Engineering, 10(2), 289-309.

@article{Ruiz-Sandoval_Tomonori_Spencer_2006, title={Sensor development using Berkeley mote platform}, volume={10}, number={2}, journal={Journal of Earthquake Engineering}, author={Ruiz-Sandoval, M and Tomonori, N and Spencer, B F}, year={2006}, pages={289--309}}



[272]

Adam Dunkels, Bjorn Gronvall, and Thiemo Voigt. 2004. Contiki - A Lightweight and Flexible Operating System for Tiny Networked Sensors. In Proceedings of the 29th Annual IEEE International Conference on Local Computer Networks (LCN '04). IEEE Computer Society, Washington, DC, USA, 455-462. DOI=10.1109/LCN.2004.38 http://dx.doi.org/10.1109/LCN.2004.38 

@inproceedings{Dunkels:2004:CLF:1032658.1034117,
 author = {Dunkels, Adam and Gronvall, Bjorn and Voigt, Thiemo},
 title = {Contiki - A Lightweight and Flexible Operating System for Tiny Networked Sensors},
 booktitle = {Proceedings of the 29th Annual IEEE International Conference on Local Computer Networks},
 series = {LCN '04},
 year = {2004},
 isbn = {0-7695-2260-2},
 pages = {455--462},
 numpages = {8},
 url = {http://dx.doi.org/10.1109/LCN.2004.38},
 doi = {10.1109/LCN.2004.38},
 acmid = {1034117},
 publisher = {IEEE Computer Society},
 address = {Washington, DC, USA},
} 




[77]

Levis, P., Madden, S., Polastre, J., Szewczyk, R., Woo, A., Gay, D., Hill, J., et al. (2005). TinyOS : An Operating System for Sensor Networks. (W. Weber, J. M. Rabaey, & E. Aarts, Eds.)Ambient Intelligence, 35(August), 115-148. Springer. Retrieved from http://www.springerlink.com/index/l82402t220708233.pdf

@article{Levis_Madden_Polastre_Szewczyk_Woo_Gay_Hill_Welsh_Brewer_Culler_2005, title={TinyOS : An Operating System for Sensor Networks}, volume={35}, url={http://www.springerlink.com/index/l82402t220708233.pdf}, number={August}, journal={Ambient Intelligence}, publisher={Springer}, author={Levis, Philip and Madden, Sam and Polastre, Joseph and Szewczyk, Robert and Woo, Alec and Gay, David and Hill, Jason and Welsh, Matt and Brewer, Eric and Culler, David}, editor={Weber, Werner and Rabaey, Jan M and Aarts, EmileEditors}, year={2005}, pages={115--148}}


[4b]

Lee, H.-C. L. H.-C., Banerjee, A., Fang, Y.-M. F. Y.-M., Lee, B.-J. L. B.-J., & King, C.-T. K. C.-T. (2010). Design of a Multifunctional Wireless Sensor for In-Situ Monitoring of Debris Flows. Ieee Transactions On Instrumentation And Measurement. Retrieved from http://www.scopus.com/inward/record.url?eid=2-s2.0-77958011806&partnerID=40&md5=f54e887a0af0a18470aded567b70ed5d

@misc{Lee_Banerjee_Fang_Lee_King_2010, title={Design of a Multifunctional Wireless Sensor for In-Situ Monitoring of Debris Flows}, volume={59}, url={http://www.scopus.com/inward/record.url?eid=2-s2.0-77958011806&partnerID=40&md5=f54e887a0af0a18470aded567b70ed5d}, number={11}, journal={Ieee Transactions On Instrumentation And Measurement}, author={Lee, Huang-Chen Lee Huang-Chen and Banerjee, A and Fang, Yao-Min Fang Yao-Min and Lee, Bing-Jean Lee Bing-Jean and King, Chung-Ta King Chung-Ta}, year={2010}, pages={2958--2967}}



[OASIS]
OASIS (2005). Common Alerting Protocol, v.1.1. Obtenido de: http://www.oasis-open.org/committees/download.php/15135/emergency-CAPv1.1-Corrected_DOM.pdf
