\chapter{Marco Teórico}
	\label{chap:marcoTeorico}
	
	%\epigraph{}{}
	
	\section{\textit{Debris flows}, \textit{mudflows}, flujos hiperconcentrados y fenómenos relacionados}
	
		\subsection{Introducción}
		
			Los \textit{debris flows} o flujos de detritos, \textit{mudflows} y flujos hiperconcentrados se refieren a movimientos de material sólido de alta concentración en torrentes montañosos a gran velocidad, por lo que resultan particularmente destructivos. \cite{Arattano_Marchi_2008} Estos fenómenos se consideran intermedios entre los deslizamientos (movimientos de masa con poca humedad) y las inundaciones.\\
			
			La diferencia entre \textit{debris flows}, \textit{mudflows} y flujos hiperconcentrados radica principalmente en la concentración de sedimentos en su composición, así como en el tamaño y composición de las partículas transportadas. En un extremo, se tienen los \textit{debris flows} o \textit{muddy streamflows}, que son corrientes de agua con sedimentos. Con una mayor cantidad de sedimento están los flujos hiperconcentrados o transicionales; en el otro extremo se tienen los \textit{debris flows} y \textit{mudflows}, en los que se tiene sobresaturación de detritos. (La diferencia entre estos dos radica en el tamaño y composición de los sedimentos).\\
			
			En los fenómenos con mucho contenido de agua, como las inundaciones o crecidas, dado que el agua es el componente que domina sobre los sedimentos (hasta un 10\% del peso o 4\% del volumen) el mismo se comportará tal como una corriente de agua. En fenómenos con mucho mayor contenido de detritos y sedimento como los \textit{debris flows} o \textit{mudflows} (60\% del volumen u 80\% de la masa) el comportamiento y la mecánica del flujo están dados principalmente por el del sedimento transportado. En medio de estos se tienen los flujos hiperconcentrados o transicionales, que tienen un comportamiento particular, muy distinto al de los dos extremos.\\
			
			Debido a que en el idioma español no existe una nomenclatura estándar para muchos de estos fenómenos (los nombres varían según el país para el mismo fenómeno, o se les llama de igual forma a fenómenos distintos) en esta tesis se utilizan nombres en inglés para el caso de términos que en español pueden causar ambigüedad.\\
			
			A continuación, se definen cada uno de estos fenómenos, así como otros conceptos relacionados a ellos.
		
		\subsection{Conceptos y definiciones}
		
			\begin{itemize}
				\item \textbf{\textit{Muddy streamflows}:} consiste en una corriente de agua que transporta sedimentos, con partículas finas en suspensión y partículas más grandes arrastradas por el lecho del río. Se comporta como una corriente de agua. Tienen menos concentración de partículas que los flujos hiperconcentrados o transicionales. \cite{sigurdsson:1999}
				
				\item \textbf{Flujos hiperconcentrados o transicionales:} consiste en una mezcla no uniforme de detritos y agua, con un contenido de agua mayor al de un \textit{debris flow} pero menor al de un \textit{muddy streamflow}; tiene un contenido de detritos entre un 20\% y un 60\%. Aunque conserva características fluviales, arrastra una enorme cantidad de sedimentos. Es movido por gravedad. \cite{sigurdsson:1999}
				
				\item \textbf{\textit{Debris flows:}} consiste en una mezcla saturada de agua de detritos y agua con alta concentración de sedimentos, movida hacia abajo por acción de la gravedad. Su comportamiento está determinado por las fuerzas relacionados con sus fases sólida y líquida. Se caracterizan por ser una mezcla bastante uniforme de fases líquida y sólida en un perfil vertical, lo que los distingue de los flujos hiperconcentrados, que son más ricos en agua. Tienen al menos un 50\% de volumen de sedimentos. \cite{sigurdsson:1999}\\
				
				Los detritos están conformados por material de baja plasticidad, productos de deslizamientos, actividad glacial, volcánico (lahares) o actividad humana (residuos de minería). También transportan material orgánico. El índice de plasticidad de los materiales el menor al 5\%. \cite{jakob2005debris}

				\item \textbf{\textit{Mudflows:}} mezcla saturada de material arcilloso a muy alta velocidad. Al igual que los \textit{debris flows}, tienen una mayor concentración de partículas que los flujos hiperconcentrados. La diferencia principal con los \textit{debris flows} consisten en el tamaño y composición de las partículas, que son más pequeñas y se componen principalmente de arcilla. (tienen menos de 50\% de contenido de grava y al menos un 5\% de arcilla). El índice de plasticidad de los materiales es de al menos un 5\%. \cite{jakob2005debris}

				\item \textbf{\textit{Debris avalanche:}} mezcla de detritos, roca y humedad movida pendiente abajo por acción de la gravedad. No es saturada en agua, la carga se soporta por medio de interacciones partícula-partícula. \cite{sigurdsson:1999}\\
				
				Este fenómeno está más relacionado con deslizamientos que con \textit{debris flows} y similares.

				\item \textbf{Lahares:} ``Lahar'' es una palabra de origen indonesio, que significa flujo de lodo. Consiste en un \textit{mudflow}, \textit{debris flow} o flujo hiperconcentrado de origen volcánico. Pueden originarse debido al contacto de lava caliente con nieve, hielo derretido o lluvia torrencial. \cite{sigurdsson:1999} También pueden tener origen no eruptivo, es decir, aunque no sean disparados por una erupción volcánica, transportan material de origen volcánico.\\
				
				El lahar puede variar, presentando varias fases en el tiempo y distancia: puede tener una o más fases de flujo (\textit{debris flow}, hiperconcentrado o \textit{streamflow}). Puede presentar también flujos piroclásticos (mezcla de roca caliente, ceniza y gas, producto de una erupción).

				\item \textbf{\textit{Jökulhlaup:}} término de origen islandés para referirse a inundaciones de origen glaciar. Incluyen los fenómenos denominados GLOF, por \textit{Glacial Lake Outburst Flood}. Aunque estos eventos consisten en inundaciones, también pueden ocasionar \textit{debris flows} y similares. Debido a la cantidad de agua que involucran, los eventos ocasionados por estos fenómenos pueden propagarse decenas de kilómetros río abajo. \cite{jakob2005debris}
			\end{itemize}
			
		
		\subsection{Proceso de formación y desarrollo de un \textit{debris flow}}
		
			Típicamente, la ruta de un \textit{debris flow} (y fenómenos similares) se divide en: zona de iniciación, zona de transporte y zona de deposición.\\
			
			A continuación, se hace una descripción del proceso, según \cite{jakob2005debris}:\\
			
			Frecuentemente, la zona de iniciación es una pendiente colapsada en el borde del canal. También pude originarse por el colapso de rellenos artificiales (carreteras por ejemplo). A veces el lecho del río puede volverse inestable debido a una precipitación extrema, por lo que el \textit{debris flow} se inicia espontáneamente. Generalmente el área de iniciación tiene una pendiente pronunciada , entre \(20^{\circ}\) y \(45^{\circ}\). En pendientes menores no existe suficiente energía potencial para iniciar un colapso de suelo granular. En pendientes mayores, generalmente la capa de suelo es muy delgada como para ser vulnerable a deslizamientos. Un deslizamiento inicial de unas decenas de metros cúbicos puede desencadenar un \textit{debris flow} de gran magnitud.\\
			
			Una vez iniciado, el deslizamiento inicial puede continuar pendiente abajo sin confinamiento, lo que produce un movimiento con características de flujo, caracterizado por un remoldeo de la masa en movimiento y un perfil de velocidad más o menos distribuido (\textit{debris avalanche}). La carga de sustrato en la ruta del flujo puede aumentar el volumen y el nivel de saturación. A esta parte se le denomina la zona de transporte. La deposición empezará una vez la pendiente del canal se haya reducido hasta cierto valor.\\

			Muchas \textit{debris avalanches} entran en canales con pendientes pronunciadas y continúan como \textit{debris flow} en la zona de transporte. En algunas ocasiones se forman represamientos con el material. La ruptura repentina y erosión de estos represamientos forman un pico del \textit{debris flow} en el canal. Los \textit{debris flow} en zonas de transporte se forman con pendientes mayores a los \(10^{\circ}\). El \textit{debris flow} va a incorporar materiales sueltos y rocas en el lecho del río. La cantidad de material incorporado por unidad de longitud del canal se llama \textit{debris yield rate} (\(^{m^{3}}/_{m}\)).\\
			
			Los \textit{debris flow} comúnmente mueven varios picos de material, separados por un flujo intermedio de agua principalmente. Un evento de \textit{debris flow} puede consistir de un pico o de cientos de ellos sucesivos.\\
			
			El cuerpo principal es una masa fina de detritos licuados. La cola o postflujo es un flujo turbulento, diluido de agua cargada de sedimento, similar a un \textit{debris flood}. El crecimiento del frente rocoso causa un aumento en el nivel del flujo tras él, lo que causa una magnificación proporcional de la descarga pico. En la figura \ref{fig:formacionDebrisFlow} se detalla el perfil del \textit{debris flow}.\\
			
			En flujos con contenido menos rocoso y de material más fino, el flujo principal en la descarga pico puede ser de naturaleza laminar, pero precedido de una fase turbulenta.

			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.9\textwidth, bb=0 0 952 430]{images/fig1-1-diagramaGeneralDebrisFlow.png}
				\\[0.2cm]
				\caption{Formación de un \textit{debris flow}. Tomado de \cite{jakob2005debris}}
				\label{fig:formacionDebrisFlow}
			\end{figure}
			
			El área de deposición principal de un \textit{debris flow} generalmente ocurre en un abanico aluvial o cono establecido. La deposición ocurre como resultado de la reducción de la pendiente y la pérdida del canal de confinamiento de la corriente. Al perder el confinamiento, la parte del pico tras el frente colapsa, por lo que el frente se queda sin su principal fuerza impulsora, con lo que pierde velocidad y es desplazado hacia las márgenes del canal, donde forma diques con el material arrastrado. Eventualmente el material diluido del pico principal desplaza el frente rocoso que formará un depósito. Este proceso puede repetirse muchas veces en la superficie del abanico, reduciendo la cantidad de clastos en el frente así como la descarga pico. Esto causa abandono del cauce principal y redirección del flujo (avulsión) en \textit{debris fans} y la formación de canales de distribución.\\
			
			En la parte próxima (cercana al canal) del abanico detritos gruesos forman altas descargas y depósitos gruesos. En la parte distante del abanico, se forman depósitos más finos y las velocidades son menores.
		
		\subsection{Sistemas de mitigación}
		
			Los riesgos por \textit{debris flows} y similares pueden ser mitigados mediante medidas activas (barreras, diques) o pasivas (como prohibir construir en zonas de riesgo). Sin embargo, no siempre es factible implementar estas medidas, por lo que se debe recurrir a sistemas de alerta temprana, basados en instrumentación de campo, que detectan el evento o las condiciones que le preceden, lo que permite tomar las medidas de respuesta que correspondan. Estos sistemas son efectivos cuando forman parte de un programa integral de defensa civil ante este tipo de amenazas. \cite{jakob2005debris}
			
			\subsubsection{Sistemas de monitoreo de antecedentes (predictivos)}
			
				Estos sistemas monitorean las condiciones hidrológicas que anteceden a un evento de este tipo, de forma que se pueda dar una alerta cuando éstas son peligrosas. Entre las variables a observar se encuentran: precipitación, presión a poro y humedad del suelo. A veces pueden monitorearse variables asociadas (por ejemplo, en un volcán, para predecir lahares se puede monitorear también la actividad volcánica) \cite{jakob2005debris}

			\subsubsection{Sistemas de detección del evento}

				Tradicionalmente la colocación de instrumentación que detecte directamente en el canal la presencia del evento ha sido complicada, por lo que se prefieren métodos de detección indirecta, a distancia del evento, pues a largo plazo resultan más confiables. \cite{jakob2005debris} Estos métodos de detección indirecta se basan en la medición de las vibraciones ocasionadas por el evento. Para detectar estas ondas se utilizan acelerómetros, sismométros y geófonos.

				En los casos en que se utiliza detección directa se utilizan medidores de nivel basados en microonda o en ultrasonido, los cuales miden el nivel del agua. También existen sensores de velocidad basados en radar, que se valen del Efecto Doppler para medir la velocidad de la superficie del flujo. También se están utilizando sistemas de video y reconocimiento de imágenes y visión por computadora para detectar el evento de forma automatizada.
				
	\section{Redes inalámbricas de sensores}
	
		\subsection{Generalidades}
			
			Estas redes consisten en pequeños dispositivos con capacidad sensorial, de procesamiento y bajo consumo de energía; inmersos en el fenómeno que se requiere observar, enlazados mediante redes inalámbricas \textit{ad-hoc}, es decir, organizadas de forma autónoma entre los dispositivos y con capacidad de reconfigurarse dinámicamente en caso de que la red cambie (por ejemplo, al quedar algún nodo fuera de ella debido al agotamiento de su fuente de energía o al ser destruido por causa de estar en un entorno hostil). En este tipo de redes la información adquirida en cualquiera de sus nodos es enrutada a través de otros nodos de la red hacia uno o varios sumideros (\textit{sinks}) que la recogen y transfiere a sus usuarios (observadores), como se muestra en la figura \ref{fig:esquemaRedSensores}.
			
			Debido a sus características, este tipo de redes resultan de gran utilidad para la observación de fenómenos ambientales, sobre un área geográfica extendida. El hecho de que puedan funcionar de forma desatendida con mantenimiento mínimo en entornos hostiles y que puedan reconfigurarse de forma dinámica los hace idóneos para aplicaciones de monitoreo de amenazas ambientales, en particular, para sistemas de predicción y detección de amenazas naturales, lo cual permitiría mejorar los sistemas de alarma ante desastres naturales.
			
			\begin{figure}[htp]
				\centering
				\includegraphics[width=\textwidth, bb=0 0 714 293]{images/fig1-2-EsquemaRedSensores.png}
				\\[0.2cm]
				\caption{Esquema de una Red Inalábrica de Sensores}
				\label{fig:esquemaRedSensores}
			\end{figure}

		\subsection{Características}
		
			Los protocolos de red y el software que se utilicen en una red de sensores deben tomar en cuenta las características particulares de las redes inalámbricas de sensores, que se listan a continuación. \cite{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002}
			
			\begin{itemize}
				\item Organización \textit{ad-hoc}.
				\item Los nodos pueden desaparecer de la red. Aun así, la red debe permanecer funcional.
				\item La energía debe conservarse al máximo.
				\item La capacidad de procesamiento de los nodos es limitada.
			\end{itemize}

		\subsection{Sensores}
			
			Son los dispositivos que se encargan de implementar el sensado físico del fenómeno ambiental y reportar los resultados. Típicamente consta de las siguientes partes: \cite{Tilak:2002:TWM:565702.565708}
			
			\begin{itemize}
				\item \textit{Hardware} de sensado.
				\item Memoria.
				\item Batería (puede también tener suministro externo de energía).
				\item Procesador
				\item Transmisor / receptor de radio.
			\end{itemize}
			

		\subsection{Modelo de capas y planos}

			En \cite{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002} se propone un modelo de cinco capas para el estudio de las redes inalámbricas de sensores. Estas capas corresponden a las capas del modelo de referencia OSI, quitándole las capas de sesión y presentación.
			
			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.6\textwidth, bb=0 0 326 298]{images/fig1-3-capasProtocoloRedSensores.png}
				\\[0.2cm]
				\caption{Modelo de capas para la red inalámbrica de sensores. Tomado de \cite{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002}}
				\label{fig:capasProtocolosRedSensores}
			\end{figure}
			
			A estas capas son ortogonales tres planos: administración de la tarea, administración de movilidad y administración de la energía, como se ilustra en la figura \ref{fig:capasProtocolosRedSensores}.
			
			\subsubsection{Capa de aplicación}
			
				Se encarga de abstraer la topología física de la red para la aplicación y proveer al software de aplicación de las interfaces necesarias para interactuar con el mundo físico a través de la red de sensores. Una forma de hacerlo es a través de \textit{queries} o consultas que el usuario puede hacer a la red de sensores, como si se tratara de una base de datos. \cite{Akyildiz_Vuran_2002}
				
			\subsubsection{Capa de transporte}
			
				Se encarga de mantener el flujo de datos (si la aplicación lo requiere) así como la multiplexación entre distintas aplicaciones y tareas. Para la capa de aplicación hay dos direcciones de transporte principales: evento-a-\textit{sink} y \textit{sink}-a-evento (consultas, peticiones) Algunos protocolos propuestos son: RMST, CODA y ESRT. \cite{Akyildiz_Vuran_2002}

			\subsubsection{Capa de red}

				Se encarga del enrutamiento de los datos dentro de la red de sensores. Los protocolos de la capa de red para redes de sensores se agrupan según \cite{Akyildiz_Vuran_2002} en las siguientes categorías:
				
				\begin{itemize}
					\item \textbf{Centrados en datos / arquitectura plana:} no hay un direccionamiento de los nodos, el enrutamiento de los datos se hace basado en la descripción de los datos transportados.
					\item \textbf{Jerárquicos:} Los sensores forman grupos (\textit{clusters}) en los cuales se elige un líder, el cual se encargará de agregar (fusionar) los datos y transportarlos hacia el \textit{sink}. Los líderes pueden formar otro grupo y así tener varios niveles en la jerarquía.
					\item \textbf{Basados en ubicación:} los protocolos de enrutamiento pueden aprovechar la información de ubicación de cada nodo en la red para optimizar las decisiones sobre las rutas.
					\item \textbf{Basados en calidad de servicio:} algunos toman en cuenta el costo energético de llevar un mensaje hasta el \textit{sink} por determinada ruta.
				\end{itemize}

			\subsubsection{Capa de enlace de datos}
			
				Se encarga del envío y recepción de datos, construcción y lectura de tramas y del acceso al medio. Para el caso de redes de sensores es crítico minimizar el consumo de energía, por lo que los protocolos en esta capa se orientan hacia ese objetivo.
			
			\subsubsection{Capa física}
			
				Se encarga de la conversión entre bits y señales de radiofrecuencia apropiadas para el canal. Se incluye aquí todo el \textit{hardware} para la transmisión de datos por radiofrecuencia.
			
		\subsection{Protocolos de red}
		
			En \cite{Tilak:2002:TWM:565702.565708}, se proponen los siguientes criterios para evaluar los protocolos de red utilizados en una red de sensores:
			
			\subsubsection{Eficiencia energética}
				
				Debido a que la energía es probablemente el recurso más limitado en una red de sensores, esta debe cuidarse en extremo para maximizar el tiempo de vida del sistema. Este puede medirse mediante parámetros genéricos como el tiempo transcurrido antes que se mueran la mitad de nodos del sistema o mediante métricas específicas de la aplicación.

			\subsubsection{Latencia o retardo}
				
				El retardo máximo requerido para la red de sensores va a depender de la aplicación específica.

			\subsubsection{Exactitud}
				
				El objetivo primario del observador es el de obtener los datos más exactos posibles. Existe un \textit{trade-off} entre exactitud, retardo y eficiencia energética. La infraestructura debería ser lo suficientemente flexible para que la aplicación obtenga los datos con la exactitud y retardo requeridos, al mínimo costo de energía. 

			\subsubsection{Tolerancia a fallas}
				
				Los sensores individuales en la red pueden fallar debido a condiciones del entorno o porque se ha agotado su reserva de energía. La red debe sobrevivir a estos fallos individuales de forma que sean ocultos a la aplicación. 
		
			\subsubsection{Escalabilidad}
			
				Es un factor crítico. En redes de gran escala es posible que esta se pueda lograr a través de jerarquización de la red y agregación de datos.\\
				
			La comunicación en una red de sensores difiere del de una red de computadoras tradicional en que la comunicación no es \textit{end-to-end}; es decir, la finalidad última de la red no es establecer un canal de datos entre dos nodos específicos de la red; sino más bien, reportar al observador información sobre el fenómeno. El observador no necesariamente conoce la infraestructura de la red ni los sensores como puntos terminales de la comunicación. \cite{Tilak:2002:TWM:565702.565708}

		\subsection{Modelos de comunicacón}
		
			En \cite{Tilak:2002:TWM:565702.565708} se habla de que la comunicación en una red de sensores conceptualmente se clasifica en dos categorías: aplicación e infraestructura. La comunicación de la categoría de aplicación se relaciona con la transferencia de datos medidos (o derivados de estos) con el objetivo de informar de ellos al observador. Dentro de la categoría de comunicación de aplicación, se tienen dos modelos: cooperativo y no cooperativo. En el modelo cooperativo los sensores se comunican entre ellos para facilitar los intereses del observador (más allá de la comunicación que existe para relevar los datos para efectos de enrutamiento) Un ejemplo de este modelo es el procesamiento de datos dentro de la red. Los sensores no cooperativos no realizan estas tareas.\\
			
			La comunicación de infraestructura se refiere a la que se realiza para configurar, mantener y optimizar la operación de la red. Debido a la naturaleza \textit{ad-hoc} de la red, es necesario que los sensores descubran rutas a otros nodos de interés. Este tipo de comunicación representa el \textit{overhead} del protocolo, por lo que se debe mantener al mínimo para que la comunicación sea eficiente.
			
		\subsection{Modelos de trasiego de datos}

			En \cite{Tilak:2002:TWM:565702.565708} se propone que las redes de sensores se pueden clasificar en términos de la entrega de datos requerida por el observador de la siguiente forma: continua, por evento, a petición del observador e híbrida, los cuales determinan la generación del tráfico de aplicación. En el modelo continuo los sensores transmiten datos de forma ininterrumpida, a un período de muestreo determinado. En el modelo por evento, los sensores transmiten datos solo si ocurre un evento de interés. En el modelo a petición (solicitud/respuesta) los sensores solo reportan datos si el observador los solicita. El modelo híbrido combina dos o más de estos modelos.\\
			
			La anterior clasificación se origina desde la perspectiva de la aplicación; desde el punto de vista de enrutamiento de datos dentro de la red, éste se puede clasificar en: \textit{flooding} (basado en \textit{broadcast} o difusión), \textit{unicast} o \textit{multicast}). El método de \textit{flooding} es el más ineficiente y de mayor \textit{overhead}, pero es inmune a cambios en la topología de red, a la vez que el \textit{overhead} puede reducirse utilizando esquemas de agregación de datos \cite{Tilak:2002:TWM:565702.565708}.\\
			
			La interacción entre el modelo de trasiego de datos de la aplicación y el del enrutamiento determinará en gran parte el desempeño de la red. Por ejemplo, en una red que envía datos por detección de evento, puede verse afectada al utilizar un esquema de enrutamiento basado en \textit{flooding}, al tratar varios nodos vecinos de reportar el mismo evento \cite{Tilak:2002:TWM:565702.565708}.\\

		\subsection{Modelos de dinámica de la red}
			
			En \cite{Tilak:2002:TWM:565702.565708} también se clasifica las redes de sensores según su dinámica de red en dinámicas y estáticas. En las dinámicas, al menos un elemento entre los siguientes: observador, fenómeno o  sensores, está en movimiento, por lo que las rutas entre el fenómeno y el observador varían en el tiempo, lo cual, debe tomarse en cuenta para el enrutamiento. En una red estática, ninguno de los elementos mencionados está en movimiento, por lo que el protocolo de enrutamiento debe generar nuevas rutas solo en caso de falla de alguna de las existentes.
			
		\subsection{Agregación de datos}
		
			Consiste en hacer procesamiento de datos en la red con el objetivo de reducir la cantidad de datos que se transmiten. Así por ejemplo, si varios sensores vecinos están midiendo temperaturas muy similares, en lugar de enviar por separado la lectura de cada uno al nodo \textit{sink}, se enviará un solo dato con la temperatura promedio de la región.
			
		\subsection{Ciberinfraestructura}
		
			Generalmente cuando se introduce el tema de redes de sensores, en la representación de las mismas en un esquema, los datos terminan en una PC o servidor que muestra los datos y a lo sumo los almacena. Sin embargo, las redes de sensores son capaces de producir una gran cantidad de datos, que en muchos casos deben compartirse con diversas organizaciones o de forma pública a través de Internet.\\
			
			Actualmente muchas tareas de análisis y visualización de datos obtenidos automáticamente a través de una red inalámbrica de sensores se exportan de forma manual a través de un sitio web. Sin embargo, esto está evolucionando hacia su realización de forma automática, mediante tecnologías de \textit{web services}, computación en GRID y la Web Semántica \cite{Hart_Martinez_2006, Akyildiz_Vuran_2002}.
			
			El término ``ciberinfraestructura'' fue acuñado por la NSF para describir nuevos entornos de investigación en los cuales, se hacen disponibles servicios colaborativos de adquisición, administración, computación y análisis de datos. \cite{Hart_Martinez_2006, Akyildiz_Vuran_2002}


		\subsection{Historia, aplicaciones y perspectivas futuras}
		
			En \cite{Chong_Kumar_2003} se ubica el inicio de la investigación en redes inalámbricas de sensores en el ámbito militar. Uno de los primeros proyectos fue SOSUS, un sistema de sensores acústicos (hidrófonos) instalados en el fondo del mar durante la guerra fría para detectar submarinos, luego se le dio otros usos por la NOAA para monitoreo de otros eventos en el océano (sismos, monitoreo animal).\\
			
			La investigación moderna en redes de sensores inicia según \cite{Chong_Kumar_2003} con el programa Distributed Sensor Networks de DARPA. La idea del mismo era extender la (entonces incipiente) Internet hasta redes de sensores. Con este programa se empieza a hacer investigaciones en el tema y algunos desarrollos importantes. La Universidad Carnegie-Mellon desarrolla un sistema operativo para sensores distribuidos, que eventualmente llega a ser el sistema operativo Mach. Se investiga también en el tema de Inteligencia Artificial, con el objetivo de procesar e interpretar automáticamente las señales recibidas.\\
			
			En las décadas de 1980 y 1990, muchos de estos avances fueron incorporados a la tecnología militar, para el desarrollo de ``armas inteligentes'' y sistemas de detección de amenazas.\\
			
			Avances recientes en las tecnologías de computación y telecomunicaciones han permitido un alto grado de miniaturización y capacidad de procesamiento en la red de sensores. El desarrollo de tecnologías de sistemas microelectromecánicos (MEMS) permite el desarrollo de sensores microscópicos.\\
			
			En la industria ya se están comercializando sistemas basados en redes de sensores para diversas aplicaciones:
			\begin{itemize}
				\item Seguridad
				\item Monitoreo ambiental
				\item Monitoreo industrial
				\item Control de tráfico
			\end{itemize}
			
			La norma IEEE 802.15 define un estándar para redes de área personal (PAN) el cual se está utilizando para la implementación de redes inalámbricas de sensores; además es la base para algunos \textit{Stacks} de protocolos inalámbricos de la industria para redes de sensores, como ZigBee, MiWi y WirelessHart.
			
		\subsection{Uso de redes inalámbricas de sensores para la detección de amenazas naturales}
		
			Para el campo de monitoreo ambiental, las redes inalámbricas de sensores representan una evolución de los registradores de datos, primero analógicos (\textit{chart recorders} y otros) y luego digitales (que requerían que personal de mantenimiento descargara de forma manual los datos almacenados). \cite{Hart_Martinez_2006}\\
			
			En \cite{Hart_Martinez_2006}, se distingue dos clases de redes de sensores ambientales: redes de gran escala de función única y redes multifunción de pequeña escala. Las primeras cubren áreas geográficas muy grandes (algunas abarcan todo el planeta) y miden pocas variables orientadas a un solo propósito. Un ejemplo de estas son las estaciones climatológicas, la red sismográfica global, SNOTEL (\textit{Snowpack Telemetry}), DART (\textit{Deep-ocean Assesment and Reporting of Tsunamis}, que da alertas tempranas ante los mismos) y TAO que obtiene datos de boyas para el estudio del fenómeno de ``El Niño''. Los segundos utilizan menos nodos y tienen un carácter más local; registran una mayor variedad de datos que pueden utilizarse para aplicaciones más diversas. Entre ellas se encuentran IPSWATCH, Great Duck Island, FloodNet.
			
			
	\section{Uso de redes inalámbricas de sensores para detección de \textit{debris flows} y fenómenos relacionados}

		La idea de utilizar sensores móviles arrastrados por la corriente para detectar este tipo de eventos fue propuesta y desarrollada por Lee en \cite{DBLP:journals/tim/LeeBFLK10}. Ahí se plantea colocar un conjunto dispositivos sensores, denominados INSIDER en el lecho del río para que sean arrastrados por la corriente en caso de un flujo de escombro. Estos dispositivos INSIDER miden parámetros internos del evento, tales como la amplitud y la frecuencia de las vibraciones, dirección de movimiento y velocidad del flujo de escombro. Los INSIDER transmiten esta información a dispositivos receptores ubicados en la margen del río, denominados COORDINATOR, los cuales se encargan de reenviar esa información al centro de recolección de datos. La figura \ref{fig:wclee} ilustra dicha propuesta.\\ 
		
		
			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.6\textwidth, bb=0 0 823 506]{images/wclee.png}
				\\[0.2cm]
				\caption{Esquema de detección propuesto en \cite{Lee:2008:UMW:1460412.1460476}}
				\label{fig:wclee}
			\end{figure}		
