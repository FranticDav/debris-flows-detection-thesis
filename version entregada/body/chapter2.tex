\chapter{Arquitectura del sistema de detección propuesto}
	\label{chap:arquitecturaSistemaDeteccionPropuesto}
	
	%\epigraph{}{}
	
	\section{Descripción general}
	
		El método propuesto tiene como finalidad detectar \textit{debris flow}, \textit{mudflows}, flujos hiperconcentrados y otros fenómenos similares en ríos, a través de una red inalámbrica de dispositivos sensores distribuidos en el cause del río, que detectan el evento al ser arrastrados por éste. Al hacerlo envían una alerta que es propagada a través de los otros nodos sensores ubicados río abajo, donde será recibida y anunciada a la población.\\

		El método sigue un paradigma de detección descentralizado; esto es, que a diferencia de enfoques como el planteado en \cite{DBLP:journals/tim/LeeBFLK10}, no depende de una entidad central con pleno conocimiento de lo que sucede en el sistema para declarar la alerta; sino que esta se genera por concenso entre los distintos nodos de la red de sensores. El criterio propuesto para este método distribuido de detección del evento es la aparición de nodos que se sabe estaban ubicados originalmente río arriba. Una consecuencia importante de este método es la de no requerir hardware o instrumentación especializada para medir alguna variable física del entorno.\\

		En la figura \ref{fig:ilustracionGeneral} se ilustra de forma general este método de detección.\\
		
		\begin{figure}[h!]
				\centering
				\subfloat[]{
					\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/fig2-1-a-ilustracionGeneral.png}
					\label{fig:ilustracionGeneral-a}
				}
				\qquad
				\subfloat[]{
					\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/fig2-1-b-ilustracionGeneral.png}
					\label{fig:ilustracionGeneral-b}
				}
				\qquad
				\subfloat[]{
					\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/fig2-1-c-ilustracionGeneral.png}
					\label{fig:ilustracionGeneral-c}
				}
				\qquad
				\subfloat[]{
					\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/fig2-1-d-ilustracionGeneral.png}
					\label{fig:ilustracionGeneral-d}
				}
				\qquad
				\subfloat[]{
					\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/fig2-1-e-ilustracionGeneral.png}
					\label{fig:ilustracionGeneral-e}
				}
				\\[0.2cm]
				\caption{Ilustración general del método de detección.}
				\label{fig:ilustracionGeneral}
			\end{figure}

		Inicialmente, los sensores se colocan dentro del cauce del río y a la orilla de éste, como se aprecia en la figura  \ref{fig:ilustracionGeneral-a}. Al ocurrir un evento como un \textit{debris flow} o \textit{mudflow}, este arrastra los sensores que se encuentran a su paso (figura \ref{fig:ilustracionGeneral-b} ). Los sensores de forma distribuida detectan el cambio en la configuracion de la red y deciden por concenso que están en presencia de un evento que debe reportarse (figura \ref{fig:ilustracionGeneral-c}) por lo que notifican a los sensores ubicados río abajo, que a su vez propagarán esa alerta hacia el resto de sensores en dirección descendente (figura \ref{fig:ilustracionGeneral-d}) hasta que todos los nodos de la red tengan conocimiento del evento (figura \ref{fig:ilustracionGeneral-e}) y alerten a la población potencialmente afectada.\\


	\section{Topología}

		Como se ha mencionado anteriormente, la colocación de los sensores en el cauce del río es tal que sean arrastrados en el caso de ocurrir un \textit{debris flow}, \textit{mudflow} o similar.  Este requerimiento define la colocación de los sensores y por ende, la topología física de la red.\\ 

		Considérese el caso simple en el que se monitorea un segmento del cause de un río, sin sus afluentes. Los sensores van a estar distribuidos a lo largo de este segmento (dentro del cause y a la orilla) como se ilustra en la figura \ref{fig:topologia-a}.\\



		Es necesario que el área cubierta por los sensores incluya las partes altas del río, lo que permitiría detectar el \textit{debris flow} con la mayor anticipación posible. También debe incluir las partes intermedias del mismo, así como las contiguas a las zonas pobladas que se requiere alertar, pues debe existir un camino para que la alerta se propague desde la zona de detección hasta los receptores de la misma.\\


		Esta disposición de sensores produce como resultado una topología de malla, cuyo tejido emula el cause del río, como se ilustra en la figura \ref{fig:topologia-b}. Cada nodo en la red está representado por un punto, las líneas representan enlaces entre nodos los suficientemente cercanos como para establecer una comunicación bidireccional directa entre ellos.\\ 

	\begin{figure}[h!]

					\qquad
					\subfloat[]{
						\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/topologia_a.png}
						\label{fig:topologia-a}
					}
					\qquad
					\subfloat[]{
						\includegraphics[width=0.45\textwidth, bb=0 0 988 623]{images/topologia_b.png}
						\label{fig:topologia-b}
					}
					\qquad
					\subfloat[]{
						\includegraphics[width=0.45\textwidth, bb=0 0 593 303]{images/topologia_c.png}
						\label{fig:topologia-c}
					}
					\qquad
					\subfloat[]{
						\includegraphics[width=0.45\textwidth, bb=0 0 598 287]{images/topologia_d.png}
						\label{fig:topologia-d}
					}
					\\[0.2cm]
					\caption{Distintos casos de la topología de red.}
					\label{fig:topologiaRed}
				\end{figure}
	
	Una vez considerado el caso simple, correspondiente a observar un segmento de río sin contemplar sus afluentes, se puede considerar el caso más general, en el que se monitorea un río principal junto con sus afluentes. La red resultante tiene la disposición geográfica mostrada en la figura \ref{fig:topologia-c}.\\
	
	
	Al igual que el caso anterior, la topología resultante emula el cause del río, esta vez conformado por su cauce principal y alimentado por varias afluentes. Nótese que la misma presenta ramificaciones, similar a una estructura de árbol, como se muestra en la figura \ref{fig:topologia-d}.\\
	

	
		\subsection{Modelo de trasiego de datos}
		
		De acuerdo con la taxonomía propuesta en [51], el modelo de trasiego de datos en la red de sensores que se propone en este trabajo es de trasiego manejado por evento: solo en caso de detectarse un \textit{debris flow} o similar se reportan datos a los usuarios de la red, no se envían datos de forma periódica ni los usuarios tienen capacidad para hacer consultas sobre el estado del área observada.\\
		
	\section{Sensores}
	
		En esta propuesta los sensores son los elementos físicos encargados de detectar la presencia del \textit{debris flow} al ser arrastrados por él.  Cada sensor estará integrado por un microcontrolador o microprocesador embebido (MCU), radio transmisor-receptor que lo comunica con otros sensores, suministro de energía y su encapsulado físico (housing) como se ilustra en la figura \ref{fig:diagramabloques}.\\
	
		% Aqui va la figura 2_3  Diagrama de bloques de un sensor  diagramabloquessensor
			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.9\textwidth, bb=0 0 650 542]{images/diagramabloquessensor.png}
				\\[0.2cm]
				\caption{Diagrama de bloques de un nodo sensor}
				\label{fig:diagramabloques}
			\end{figure}
	
		Como se ha indicado, el objetivo de este trabajo es hacer una propuesta general y desarrollar y evaluar los algoritmos y protocolos para un sistema de detección. El diseño y especificación de los sensores para una implementación real queda fuera del alcance del mismo. Sin embargo, se pueden enumerar algunas características requeridas para los sensores, de acuerdo al papel que juegan en la estrategia de detección propuesta.\\

		Cada sensor deben ser de bajo costo, pues deben colocarse muchos de ellos a lo largo del cauce del río. Además, debido a las características del planteamiento, los sensores no son reutilizables, en el sentido de que no resulta práctico recuperarlos luego de un \textit{debris flow}. Cuando se ha detectado un evento, deberá implantarse una nueva red de sensores en el río; por lo que también esta implantación debe ser lo más sencilla y rápida posible.\\

		El suministro de energía es un factor determinante para los sensores, pues una vez que este se agote el sensor será inoperante en la red. Esto debe ser tomado en cuenta para determinar el tiempo de vida de la misma. \\

		Aunque existen métodos para extraer energía del medio ambiente donde se encuentra el sensor éstos pueden elevar el costo de cada sensor y hacerlos suceptibles a vandalismo; por lo que para efectos de este trabajo se supondrá que el suministro de energía será dado únicamente por una batería en el sensor. Por tanto, la presente propuesta se encaminará a economizar al máximo este recurso.\\

		El diseño y construcción física de los sensores debe ser tal que durante el comportamiento normal del río estos permanezcan estáticos donde fueron ubicados y que al ocurrir un evento como un \textit{debris flow} o \textit{mudflow}, sean arrastrados por el mismo. Además, deben  soportar las condiciones físicas ambientales del entorno en que serán ubicados, durante el tiempo de vida requerido.\\

		Estos sensores pueden estar basados en motes incluyendo plataformas comercialmente disponibles, tales como Telos \cite{Polastre:2005:TEU:1147685.1147744} o Mica\cite{Xu02asurvey}, como el de la fotografía de la figura \ref{fig:mote}.\\


		% Aqui va la figura 4  fotografia de un nodo sensor 		mote.jpg
			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.9\textwidth, bb=0 0 400 286]{images/mote.jpg}
				\\[0.2cm]
				\caption{Fotografía de un mote. (Intel Research)}
				\label{fig:mote}
			\end{figure}
	

		Debido al enfoque descentralizado propuesto, la plataforma de hardware y software debe ser capaz de formar redes \textit{peer to peer} de forma autónoma, sin depender de un coordinador. De lo contrario la red resultante dependerá de un elemento central, por lo que su tolerancia a fallas se verá comprometida; o su implementación podría tornarse imposible en el campo, debido a que puede no existir conectividad inalámbrica entre el coordinador y cada uno de los sensores.\\

		Los sensores requeridos para esta propuesta no requieren instrumentación o hardware especializado para medición de variables físicas. La estrategia de detección está basada en detectar la proximidad de otros sensores que se sabe estaban ubicados río arriba como producto de su arrastre por el \textit{debris flow}. Esta estrategia no excluye el uso de acelerómentros u otros elementos sensores de movimiento; pero la estrategia aquí propuesta no supone ni requiere su presencia.\\


	\section{Estrategia de detección}

		En esta propuesta, se han considerado dos  aproximaciones o enfoques. En una primera aproximación, con el fin de simplificar el análisis, se parte de un modelo muy básico de propagación de radio, que ignora fenómenos como interferencia, múltiples caminos u obstáculos. En una aproximación posterior se tomará en cuenta estos fenómenos, para que los algoritmos y protocolos propuestos puedan funcionar correctamente en una implementación real.\\

		\subsection{Primera Aproximación}
		
			El modelo básico de propagación de señal de radio que se utilizará en una primera aproximación al problema presupone las siguientes características:

			\begin{enumerate}
				\item Los efectos de reflexión, refracción o difracción ocasionados por obstáculos o relieve se consideran despreciables y se ignora su efecto en la propagación de la señal.\\
		
				\item La potencia de la señal recibida en un nodo sensor, proveniente de otro; ambos con radiadores isotrópicos (antenas omnidireccionales) es función solamente de la potencia de la señal emitida y de la distancia entre ambos sensores.\\
	
				\item Los enlaces bidireccionales son simétricos: Si un nodo sensor "A" puede recibir datos del nodo "B"; entonces "B" puede recibir datos de "A"\\

				\item Como consecuencia de las características 1,2 y 3, para cada nodo se define un área de cobertura circular, para la cual los nodos ubicados dentro de ella podrán comunicarse con el nodo y viceversa. Fuera de ella no es posible la comunicación directa con el nodo en ningún sentido. El radio de esta círculo depende solamente de la potencia de los nodos.\\

			\end{enumerate}
		
		
			Suponiendo el modelo básico con las características anteriormente mencionadas, la estrategia de detección propuesta para el evento es a grandes razgos la siguiente. El algoritmo se detalla en el Capítulo 4.\\

			Considérese una red de sensores que monitorea cierto río. Un sensor ubicado en esta red, podrá tener comunicación inalámbrica con algunos de sus nodos vecinos, como se ilustra en la figura \ref{fig:estrategia_deteccion_a}. El círculo representa el área de cobertura de su radio transmisor-receptor, y las líneas rectas, los enlaces de comunicación con sensores vecinos. En el caso de los vecinos inmediatos, la intensidad de la señal recibida de ellos será mucho más fuerte y constante que para el caso de los vecinos no tan cercanos, en cuyo caso la señal recibida será más debil y en algunos casos puede dejar de recibirse ocasionalmente, segun condiciones ambientales. \\

		\begin{figure}[h!]

						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 463 283]{images/estrategia_deteccion_a.png}
							\label{fig:estrategia_deteccion_a}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 464 282]{images/estrategia_deteccion_b.png}
							\label{fig:estrategia_deteccion_b}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 465 283]{images/estrategia_deteccion_c.png}
							\label{fig:estrategia_deteccion_c}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 463 282]{images/estrategia_deteccion_d.png}
							\label{fig:estrategia_deteccion_d}
						}
						\\[0.2cm]
						\caption{Estrategia de detección.}
						\label{fig:estrategia_deteccion}
					\end{figure}

			Ahora considérese ese mismo sensor de esa red, en presencia de un \textit{debris flow} (figura \ref{fig:estrategia_deteccion_b}). El flujo arrastrará primero los sensores ubicados en las parte alta del cauce del río. Cuando el flujo junto con los sensores ubicados originalmente río arriba se aproximen al sensor en cuestión, el mismo observará lo siguiente: sensores que según la tabla estaban ubicados río arriba y que anteriormente estaban fuera de la zona de comunicación inalámbrica del sensor se hacen presentes en ella, es decir, el sensor captará su señal inalámbrica y su intensidad será creciente conforme se aproximen al sensor. Cuando el evento se aproxima al sensor lo suficiente para arrastrar a los nodos vecinos que estaban en su zona de cobertura inalámbrica (figura \ref{fig:estrategia_deteccion_c}), el nodo experimentará un aumento abrupto en la intensidad de la señal proveniente de ellos, que además crecerá conforme se aproxime el flujo. Esto le indicará al nodo que está a punto de ser arrastrado por el mismo.\\

			Una vez arrastrado por el flujo el sensor recibirá la señal de los otros sensores que han sido arrastrados, con intensidad mas o menos constante, durante su trayecto río abajo (figura \ref{fig:estrategia_deteccion_d}). También comenzará a observar que se incrementa la intensidad de la señal proveniente de los nodos vecinos río abajo, conforme el flujo se aproxima a ellos; a la vez que se empieza a detectar la señal de otros nodos ubicados más lejos río abajo, que anteriormente estaban fuera de su zona de recepción inalámbrica.\\

			El observar este comportamiento es una condición para que el nodo decida que está en presencia de un evento; sin embargo, es posible también que el sensor observe actividad similar a esta como producto de otro evento no relacionado con un \textit{debris flow}, como por ejemplo, que por actividad humana un sensor haya sido sacado de su ubicación original y sea transportado  río abajo; en cuyo caso el sensor detectará un falso positivo. También existe la posibilidad de falsos negativos, si por alguna razón el sensor no fue arrastrado por el evento. Es por esto que es necesario que cuando algun sensor detecte un indicio de evento, lo corrobore con información de nodos vecinos para decidir si se está o no en presencia de un evento y disparará o no la alerta correspondiente.\\
		
		\subsection{Segunda Aproximación}
		
			En la sección anterior se planteó una estrategia de detección dependiente de un modelo muy simple de comunicación inalámbrica; específicamente, supone como ciertas y depende fuertemente de las caracteríticas del modelo simple de propagación inalámbrica que se indicaron en dicho apartado.\\

			Como se indica en \cite{Kotz_Newport_Elliott_2003} estos supuestos que aquí se hacen, en condiciones reales no se cumplen; por lo que recomienan evitar estos supuestos, así como adoptar modelos realistas de propagación de radiofrecuencia y simulaciones en espacios tridimensionales. Como estos aspectos de modelado y simulación de propagación de señal de radiofrecuencia se han dejado por fuera del alcance de este trabajo, no se profundizará sobre ellos aquí.\\ 

			Sin embargo, sí se hará un planteamiento alternativo de estrategia de detección, que no depende de las características mencionadas; es decir, no supone que el efecto de los obstáculos es despreciable, ni que la potencia de las señales recibidas dependen solamente de la distancia a cada nodo emisor, ni enlaces simétricos o áreas de cobertura circulares.\\

			
			\subsubsection{Supuestos sobre señal de radiofrecuencia}
			
				Si bien ya no se hacen los supuestos anteriormente indicados, sí se hacen los siguientes, los cuales pueden considerarse más realistas y generales:\\

				\begin{enumerate}
				
	  				\item Para un nodo "A", la probabilidad de escuchar a otro nodo "B" es inversamente proporcional a la distancia entre ellos (o al cuadrado de ella)\\

	 				\item Cada nodo cuenta con un rango máximo, es decir, una distancia máxima tal que cualquier otro nodo ubicado a una distancia mayor no podrá comunicarse con él, aun con condiciones ambientales favorables.\\

				\end{enumerate}

				Nótese que en (2) no se garantiza la conexión a distancias menores, esta puede no darse. Solo se garantiza la no-conexión a distancias mayores\\

		
		
			\subsubsection{Estrategia de detección}
			
				Sobre estos supuestos se plantea la siguiente estrategia.\\

				Considere un nodo situado en el cauce del río, en ausencia de \textit{debris flow}. Al igual que en el caso simplificado, tendrá comunicación directa con algunos de sus nodos vecinos, aunque no necesariamente con todos sus vecinos más cercanos. Es posible que la comunicación con algunos de ellos sea intermitente, segun la calidad del enlace, aunque en este caso, no se puede hacer inferencias sobre la distancia a los nodos con enlaces intermitentes con respecto a la distancia hacia otros nodos con enlaces más estables.\\

				El comportamiento esperado para la topología de la red de sensores en ausencia de \textit{debris flow} o similar (o régimen estacionario o estable) es el de una topología más o menos estable, con enlaces en su mayor parte bien definidos y algunos que pueden presentar intermitencia, pero sin cambios significativos a lo largo del tiempo.\\

				Ahora considerese el caso de la misma red en el río en presencia de un \textit{debris flow}. Al ser arrastrados los nodos ubicados en la parte alta del río, estos experimentarán contínuamente cambios drástricos en su configuración de vecinos: nodos con los que se tenía comunicación más o menos estable pasarán a verse de forma intermitente, respondiendo al comportamiento caótico que se puede esperar de objetos arrastrados por un \textit{debris flow}. Nodos río abajo, con los que no se tenía comunicación del todo, podrían aparecer a la vista del nodo cuando este se le aproxima, muy posiblemente, de forma intermitente.\\

				Así pues, el comportamiento esperado para la red en régimen dinámico o transitorio, es decir, en presencia de un \textit{debris flow}, es el de presentar cambios abruptos en su topología, inicialmente en los nodos ubicados río arriba y luego en el resto de la red. Estos cambios son abruptos, constantes y caóticos.\\

				Así pues, la estrategia de detección consiste en detectar estos cambios abruptos en la topología de la red, corroborar esta información con la de otros nodos, si es posible y alertar a los nodos río abajo, para que propaguen la alerta.\\
			
			
			
	\section{Estimación de posición y velocidad del fenómeno}	
	
	
		\subsection{Primera aproximación}
		
			Volviendo al modelo simplificado de red inalámbrica, si se pudieran suponer ciertos los enunciados de la sección 4.1, se podría poponer la siguiente estrategia para estimar la ubicación y por lo tanto, la velocidad del \textit{debris flow}: \\
		

			En el apartado anterior se hizo una descripción de cómo podría un sensor concluir que está en presencia de un evento de \textit{debris flow}. Ahí se indicó que cuando el sensor detecta que la señal de radiofrecuencia de sus nodos vecinos río arriba se recibe con mayor potencia, el fenómeno está a punto de pasar por el sensor. Una vez que la potencia de la señal recibida de estos nodos vecinos deja de aumentar y el sensor empieza a detectar que la potencia de la señal de los nodos vecinos río abajo empieza a aumentar, el sensor puede concluir que ya ha sido alcanzado por el flujo y ha sido arrastrado por él. \\

			Así pues, el instante t en que el flujo pasó por donde estaba ubicado el sensor puede acotarse en el intervalo de tiempo intermedio entre esos dos estados, el inmediatamente anterior a la llegada del evento y el inmediatamente posterior.  Si la ubicación exacta del sensor es conocida, se puede aproximar a esa ubicación la posición del evento en ese instante t.\\

		
			Sin embargo, en una red inalámbrica de sensores ad-hoc no se cuenta con garantía de contar con la localizacion de todos los sensores en la red; la implantación de los mismos se haría muy costosa y complicada; cosa que se quiere evitar en esta propuesta.\\

			Una posibilidad para superar este escollo, es la de colocar los sensores a intervalos de longitud regulares. Como cada sensor tiene conocimiento de la distancia al último nodo de la red, al conocerse la longitud del intervalo implicitamente también se conocería la distancia desde ese último nodo hasta la posición actual del \textit{debris flow}. No se requiere que estos intervalos de longitud sean exactos, puesto que lo que interesa conocer sobre la posición del evento es una aproximación gruesa que permita a los receptores tener una idea de cuánto tiempo se dispone. En la figura \ref{fig:colocacionintervaloregular} se ilustra esta idea; los sensores se colocan con una separación cercana al valor definido por la longitud X.\\

			% <<fig 6>>  colocacionintervaloregular
			\begin{figure}[htp]
				\centering
				\includegraphics[width=0.9\textwidth, bb=0 0 500 395]{images/colocacionintervaloregular.png}
				\\[0.2cm]
				\caption{Colocación de sensores a intervalos regulares}
				\label{fig:colocacionintervaloregular}
			\end{figure}
	
			
			Si se desea contar con un dato más preciso sobre la posición del \textit{debris flow}, cabe la posibilidad de colocar algunos sensores en posiciones cuya ubicación exacta es conocida y alimentar estos datos a la red.   \\ 

			Si se conoce la posición del evento en varios instantes cercanos de tiempo t: {x0(t0), x1(t1),...xn(tn) } es posible estimar la velocidad promedio del flujo en ese intervalo, utilizando relación:  v = distancia / tiempo \\

	 		Estos datos estimados de posición y velocidad se propagarán constantemente en la red al ser detectado un \textit{debris flow}, de forma que cada estación receptora pueda estimar el tiempo restante para la llegada del evento. \\

			En  el capítulo 4 se expone con detalle los algoritmos para la estimación de la posición y la velocidad del \textit{debris flow}, con sus consideraciones de incertidumbre. \\
			
		\subsection{Segunda aproximación}
		
				Una vez descartados los postulados de la sección 2.4.1, la estrategia de estimación de posición del flujo planteada en la sección anterior no resulta confiable, pues supone que habrá una correspondencia entre la topología lógica de la red y la topología física planteada; lo cual no se puede asegurar que ocurra en la realidad. Además, depende del comportamiento descrito en el apartado 4.1, el cual supone ciertos los postulados ahí indicados, con los que tampoco se puede contar en la realidad.

				Plantear una estrategia de localización del fenómeno, utilizando unicamente los supuestos planteados en 2.4.2 con la red de sensores planteada resulta en extremo difícil, por lo que no se propone aquí una solución que satisfaga dicho problema.

				Lo que se propone aquí como alternativa para estimar la posición y velocidad del fenómeno detectado es usar una topología de red conocida a priori, con conocimiento previo de la localización de cada nodo; o bien, el uso de beacons, nodos clave de los que se conoce su localización (establecida a priori o que cuenten con hardware de geolocalización (gps o similar). Si bien esto encarecería o haría menos simple la implementación de la red, le permitiría cumplir con el requisito de estimar la posición del \textit{debris flow}, si esto es realmente requerido.
		
	\section{Esquema de propagación de la alerta}
	
		Una vez declarada la alerta entre los sensores proximos a ella, esta será propagada hacia los sensores ubicados río abajo. Para esto, cada sensor que genere o reciba una alerta, la enviará a todos sus vecinos.\\
		
		\begin{figure}[h!]

						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 601 398]{images/esquema_propagacion_a.png}
							\label{fig:esquema_propagacion_a}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 598 400]{images/esquema_propagacion_b.png}
							\label{fig:esquema_propagacion_b}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 597 344]{images/esquema_propagacion_c.png}
							\label{fig:esquema_propagacion_c}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 602 399]{images/esquema_propagacion_d.png}
							\label{fig:esquema_propagacion_d}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 600 399]{images/esquema_propagacion_e.png}
							\label{fig:esquema_propagacion_e}
						}
						\qquad
						\subfloat[]{
							\includegraphics[width=0.45\textwidth, bb=0 0 601 401]{images/esquema_propagacion_f.png}
							\label{fig:esquema_propagacion_f}
						}
						\\[0.2cm]
						\caption{Esquema de propagación de la alerta.}
						\label{fig:esquema_propagacion}
					\end{figure}
	
		Este procedimiento se ilustra en la figura \ref{fig:esquema_propagacion}. Inicialmente los sensores ubicados en la parte alta del río son arrastrados por el flujo (figura \ref{fig:esquema_propagacion_a}). Entonces los sensores ubicados en la frontera entre el \textit{debris flow} y el cauce normal del río detectan  y declaran la presencia del evento (figura \ref{fig:esquema_propagacion_b}). Cada uno de ellos notificará la alerta a los sensores vecinos ubicados en dirección río abajo (figura \ref{fig:esquema_propagacion_c}) Al recibir la alerta, cada uno de los sensores la reenviará a todos sus vecinos dentro de su área de cobertura en esa dirección (figura \ref{fig:esquema_propagacion_d}). Cada sensor que reciba la alerta repetirá este procedimiento (figura \ref{fig:esquema_propagacion_e}) hasta que todos los sensore abajo del \textit{debris flow} tengan conocimiento de la presencia del evento (figura \ref{fig:esquema_propagacion_f}).\\


	
	
	\section{Recepción de la alerta}
	
		Mediante el esquema de propagación propuesto, la alerta se diseminará al resto de nodos en la red ubicados río abajo.\\
		
		A la orilla del río, en las zonas pobladas donde quiere alertar en caso de un evento se instalarán los receptores de la alerta. Estos receptores contarán con una interfaz de radio compatible con los del resto de sensores en la red; de forma que para efectos prácticos, forman parte de la red de sensores, con la salvedad que no se toman en cuenta para determinar si ocurrió un evento, siguiendo la estrategia descrita. Es posible, construir estos dispositivos basándose en la misma plataforma de hardware y software que los nodos sensores.\\

 		A diferencia de los nodos sensores, estos no se contruirán para ser arrastrados por el río; sino que deberán construirse de tal forma que puedan ubicarse cerca de la margen del río para recibir la alerta y contar con un mecanismo de alerta a la población cercana (por ejemplo, una sirena). \\

		El detalle de estos receptores y sus especificación, así como el detalle del mecanismo de alerta a la población afectada escapa al alcance de este trabajo. \\
	
	
	\section{Infraestructura de información}	
		
		Se entiende que una implementación real en campo de un sistema de detección basada en el método aquí planteado debe contar con una infraestructra de información que permita realizar las siguientes tareas, a partir de los datos y alertas recibidas desde la red de sensores:\\

		En caso de una alerta, suministrar en tiempo real toda la información pertinente a las autoridades encargadas de atender la emergencia, lo cual les permitirá tomar mejores decisiones y que la ayuda se canalice de la mejor forma.\\

   		Monitorear el estado de la red, no solo durante un evento, también en operación nornal; lo que permitiría dar el mantenimiento adecuado a la misma para maximizar su vida útil. \\

		Como este trabajo se enfoca en plantear una estrategia general sobre la detección del fenómeno, así como profundizar en los algoritmos y protocolos necesarios para hacerlo; el diseño de una infraestructura de información, genérica o propia de alguna implementación partícular, no está contemplado en el alcance del mismo.\\  