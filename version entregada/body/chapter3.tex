\chapter{Mapa de ruta hacia un sistema real de detección de \textit{debris flows} basado en el método propuesto}
	\label{chap:mapaRutaSistemaRealDeteccion}
	
	%\epigraph{}{}
	
	\section{Introducción}
	
		Como se ha dicho anteriormente, en este trabajo se propone en términos generales el método de detección y se profundiza en los algoritmos y protocolos requeridos para el mismo, aunque se deja por fuera del alcance del mismo varios problemas que deben ser resueltos para tener una solución funcional para un río específico. En este capítulo se identifican los mismos, con el objetivo de orientar el trabajo futuro que se debe hacer a partir de este, para hacer posible la visión de contar con implementaciones reales del método aquí propuesto.\\
		
		Estos retos o problemas se han clasificado en dos grandes grupos: problemas de tipo tecnológico y problemas de tipo no tecnológico.\\
		
		Como su nombre lo indica, los primeros son problemas que se han identificado cuyo origen es meramente científico-tecnológico. Los segundos, son aspectos organizativos, administrativos o de relación con la comunidad. 

	\section{Problemas de tipo tecnológico.}
	
		\subsection{Aspectos de conectividad y radiofrecuencia}
		
			Se incluyen aquí los problemas relacionados con propagación de señal de radiofrecuencia y la conectividad de la red.\\
			
			Este es un aspecto de mucha atención en esta aplicación, pues los dispositivos emisores y receptores pueden encontrarse inmersos en una mezcla de lodo, detritos y agua; por lo que se hace necesario estudiar su comportamiento en estas condiciones con el fin de validar los supuestos que se hacen en este trabajo al plantear los algoritmos de detección; o si por el contrario, deben replantearse para que puedan funcionar en condiciones reales.\\
			
			En particular, deben estudiarse los siguientes aspectos:
			
			\subsubsection{Modelo de propagación de señal en un escenario de \textit{debris flows} y similares}
			
				En \cite{Kotz_Newport_Elliott_2003} se afirma que frecuentemente se utilizan modelos extremadamente simplificados y por lo tanto, no realistas, de propagación de señal de radio en simulaciones sobre redes inalámbricas. En ese trabajo se cuestionan los siguientes ``axiomas'' (llamados así de forma irónica por los autores), que comúnmente se suponen ciertos:
				
				\begin{itemize}
					\item La tierra es plana.
					\item El área de alcance de un radio es circular.
					\item Si te puedo oír, me puedes oír (simetría).
					\item Si te puedo oír del todo, te puedo oír perfectamente
					\item La potencia de señal recibida es una simple función de la distancia.
				\end{itemize}
				
				Dado que esta tesis hace un primer planteamiento de estrategia de detección distribuida y como tal, no pretende profundizar en temas de radiofrecuencia ni modelos de propagación; se hacen algunas simplificaciones de las señaladas en \cite{Kotz_Newport_Elliott_2003}. Sin embargo, se entiende que este aspecto es de suma importancia para el funcionamiento de un sistema de detección real, por lo que debe ser tomado en cuenta para su construcción.\\
				
				Así pues, debe trabajarse en la adopción de un modelo formal de propagación de radio para los nodos de la red de sensores, que tome en cuenta las características relevantes de la topografía de un río, así como las condiciones físicas del entorno, tanto en régimen estacionario como durante un evento.\\
				
				En trabajos como el de \cite{Zhou_He_Krishnamurthy_Stankovic_2004} se investiga el impacto de la irregularidad en la propagación de señales de radio en aplicaciones de redes de sensores; sin embargo, estudiar la propagación de señales de radio en agua, lodos o detritos, en una topografía irregular como lo es la de un río, es un problema muy particular. Debe estudiarse cómo impacta en la estrategia y algoritmos propuestos para la detección y estimación de posición. Si es necesario, deberán plantearse los ajustes que se requieran o si es del caso, replantearlos para que se ajusten a un modelo más fiel a la realidad.

			\subsubsection{Aspectos de conectividad de la red}
			
				Conectividad se refiere a la característica de la red de sensores de permanecer conectada; es decir, en ninguna configuración de nodos activos debería estar particionada. Este aspecto no debe confundirse con el de cobertura, que se refiere más bien a la calidad del monitoreo de la red de sensores sobre el fenómeno observado en una región determinada; \cite{Wang_Xing_Zhang_Lu_Pless_Gill_2003} aunque ambos conceptos guardan relación, como se explica en \cite{Wang_Xing_Zhang_Lu_Pless_Gill_2003, Zhang_Hou_2005}.\\
				
				Con respecto a este punto y basado en el modelo de propagación anteriormente mencionado, debe estudiarse cuál debe ser la separación óptima entre sensores, de tal forma que se tenga un balance entre la potencia de transmisión para que el consumo de energía sea mínimo, y el número de sensores que deben colocarse en un río para que este quede cubierto. En el trabajo realizado en \cite{Zhang_Hou_2005,Younis_Akkaya_2008,Tang_Hao_Sen_2006} se explora este problema.\\
				
				La separación entre los sensores incide en la potencia de transmisión de los sensores: a menor distancia, menor debe ser la potencia y por lo tanto, menor consumo de energía, lo que permite alargar el tiempo de vida de la red o disminuir el tamaño de la batería; pero requiere de más sensores para cubrir la misma longitud en el río. Por lo tanto, debe encontrarse un óptimo en la separación que minimice el costo de implementar una red.

			\subsubsection{Frecuencia de radio}
			
				Típicamente, las redes inalámbricas de sensores operan en las bandas ISM (\textit{Industrial, Scientific, Medical}) del espectro radioeléctrico; a saber: 900 MHz (esta banda no está disponible para este propósito en Europa) 2.4 GHz y 5 GHz. Cada una de estas bandas tiene sus características de propagación; por lo que también debe estudiarse cual de ellas resulta más adecuada al problema en cuestión.\\
				
				Aunque en una primera aproximación podría pensarse que la banda de 900 MHz tiene mejores características de propagación; esto debería estudiarse más a fondo, tomando en cuenta los requerimientos de la aplicación. Por ejemplo, podría ser que una peor propagación sea más beneficiosa para detectar la presencia del \textit{debris flow} al ver nodos río arriba que se acercan.
				
		\subsection{Plataforma de \textit{hardware} y \textit{software}}
		
			Se entiende como plataforma de \textit{hardware} y \textit{software}, la arquitectura de \textit{hardware} de referencia que se utilizará como base para los sensores; así como el \textit{software} básico: sistema operativo, servicios, bibliotecas y \textit{middleware} que se ejecuta en los nodos de la red, así como las herramientas de desarrollo o ``\textit{toolchain}'' sobre las que se construirá el \textit{software} específico para esta aplicación.\\
			
			Aunque en este trabajo se hace una implementación de los algoritmos y protocolos planteados y se construye un prototipo, esa implementación se hace como una prueba de concepto, con el objetivo de evaluar dichos algoritmos y protocolos. No se pretende que el \textit{hardware} utilizado ni la implementación de \textit{software} sean utilizados directamente en un sistema de monitoreo final; por lo cual, para dicha implementación deberá definirse y desarrollarse su \textit{hardware} y \textit{software}.\\
			
			Además de los aspectos propios de \textit{hardware} y \textit{software}, la definición de dicha plataforma, para la implementación de un sistema basado en esta propuesta implica también definir ciertas características de la red de sensores, tales como:
			\begin{itemize}
				\item Posibles topologías: física y lógica de la red.
				\item Protocolos: Control de Acceso al Medio (MAC), red, transporte.
				\item Esquemas de direccionamiento y enrutamiento.
				\item Tamaño y escalabilidad de la red.
				\item Frecuencias de operación.
			\end{itemize}
			
			Algunas de estas características ya están definidas en algunos estándares como IEEE 802.15.4 (\url{http://www.ieee802.org/15/pub/TG4.html}); o bien, Zigbee (\url{http://www.zigbee.org/}) o MiWi (\url{http://ww1.microchip.com/downloads/en/AppNotes/01204a.pdf}), que lo extienden. 		

			\subsubsection{Plataforma de Hardware}
			
				(Definición de Mote \textit{platform}: Telos, Mica, etc.)\\
				
				La plataforma de \textit{hardware} es un diseño de referencia de la arquitectura del\textit{hardware} (electrónica) del sensor: Microprocesador o microcontrolador, radio transmisor-receptor, interfaz de entradas/salidas, etc.\\
				
				Las plataformas para redes de sensores se distinguen de otras plataformas de cómputo, principalmente por el énfasis en la administración de la energía. En \cite{Hill:2004:PEW:990680.990705}, se distingue cuatro tipos de plataformas orientadas a redes de sensores: sensores especializados o \textit{minimotes} (ej \textit{RFID tags}), motes de propósito general, sensores de alto ancho de banda (aplicaciones con audio o video) y \textit{gateways} o pasarelas, que establecen enlaces entre redes de motes y redes de propósito general como redes LAN o Internet. Los sensores a los cuales se orienta esta propuesta son los motes de propósito general, aunque los dispositivos receptores de la alerta podrían considerarse como dispositivos \textit{gateway}.\\
				
				Existen tanto plataformas comerciales, como son Mica (\url{http://www.memsic.com/support/documentation/wireless-sensor-networks/category/7-datasheets.html?download=148\%3Amicaz}), Mica2 (\url{http://www.memsic.com/support/documentation/wireless-sensor-networks/category/7-datasheets.html?download=147\%3Amica2}), TelosB (\url{http://www.memsic.com/support/documentation/wireless-sensor-networks/category/7-datasheets.html?download=152\%3Atelosb}); así como también diseños de referencia abiertos que se pueden adoptar para esta iniciativa, como Berkeley Mote \cite{Ruiz-Sandoval_Tomonori_Spencer_2006} (algunas plataformas comerciales están basadas en diseños abiertos); aunque también cabe la posibilidad proponer un nuevo diseño para este sistema, si se considera que esto conlleve ventajas con respecto a adoptar una. Al adoptar una plataforma externa, ya sea comercial o abierta, se tiene la ventaja de que se puede adoptar un \textit{stack} de \textit{software} (sistema operativo, bibliotecas) que se sabe funcionará correctamente sobre dicha plataforma.

			\subsubsection{Plataforma de \textit{software}}
			
				La plataforma de \textit{software} para los sensores se conformaría por el \textit{software} que se ejecutará en ellos que permiten y facilitan la implementación de los algoritmos y protocolos de detección, estimación y alerta de \textit{debris flows} propuestos aquí. Es importante aclarar que la implementación de los algoritmos y protocolos se consideran como \textit{software} específico de la aplicación de detección de \textit{debris flows}; por lo que no forman parte de la plataforma de \textit{software}.\\
				
				Así pues, la plataforma de \textit{software} incluye: sistema operativo, servicios, bibliotecas y \textit{middleware}, que permiten el desarrollo de \textit{software} específico para la aplicación. También incluye las herramientas de desarrollo o ``\textit{toolchain}'' utilizadas para desarrollar aplicaciones para dicha plataforma: IDEs, compiladores, \textit{debuggers}, simuladores o emuladores, que no se ejecutan en los sensores, pero resultan necesarios para desarrollar aplicaciones.\\
				
				Al igual que el \textit{hardware} de referencia, existen tanto plataformas libres o de código abierto, como propietarias o privativas. Entre las plataformas de \textit{software} abiertas resaltan TinyOS \cite{Levis_Madden_Polastre_Szewczyk_Woo_Gay_Hill_Welsh_Brewer_Culler_2005} y Contiki \cite{Dunkels:2004:CLF:1032658.1034117}. La elección de la plataforma de \textit{software} está estrechamente ligada a la elección de plataforma de \textit{hardware}, debido a que el \textit{software} presupone una arquitectura de microprocesador y ciertos recursos de \textit{hardware}. Por esta razón, resulta válido hablar de una sola plataforma integral de \textit{hardware} y \textit{software}.\\
				
				La plataforma de \textit{software} debería de ocuparse de las funciones de bajo nivel requeridas por la aplicación, como lo son:
				\begin{itemize}
					\item Manejo del protocolo de control de acceso al medio (MAC).
					\item Manejo de comunicaciones en la red. Direccionamiento y enrutamiento de datos.
					\item Abstracción de los recursos de \textit{hardware}: radio, sensores físicos, procesador.
				\end{itemize}

		\subsection{Encapsulado del sensor}
		
			El método de detección propuesto en este trabajo consiste fundamentalmente en detectar el movimiento de los sensores cuando estos son arrastrados por el flujo, sea este \textit{debris flow}, \textit{mudflow} o flujo hiperconcentrado. Esta mecánica de detección impone un requisito físico fundamental a los sensores: su construcción debe ser tal que permanezcan inmóviles durante el curso normal del río, pero que en un evento de \textit{debris flows} o similar, sean arrastrados por el flujo.\\
			
			Este aspecto es crucial para lograr una implementación exitosa de un sistema de detección de este tipo; por lo que debe investigarse a fondo.\\
			
			Además, el nodo debe soportar y proteger a la electrónica interna de las condiciones físicas del entorno donde desempeñará su función. También deberá ser capaz de resistir las condiciones que se presenten al ser arrastrado por el flujo: aceleraciones, impactos, inmersión en agua y lodo.\\
			
			La idea propuesta en este trabajo, de utilizar sensores colocados en el río, que detecten el \textit{debris flow} al ser arrastrados por él; es similar a la ya formulada por \cite{DBLP:journals/tim/LeeBFLK10}, que debe enfrentar retos de naturaleza similar a los aquí expuestos en cuanto a características físicas del sensor. Dado que en ese trabajo se ha probado exitosamente un prototipo físico construido para ese entorno, sus aportes al respecto son en extremo valiosos para este trabajo.\\
			
			Un aspecto importante que debería tomarse en cuenta para el diseño del encapsulado de cada sensor es el de construirlo de la forma menos susceptible posible al vandalismo y robo; evitando materiales o componentes con alto valor de reventa y camuflándolo en el entorno del río.\\
			
			Para este problema, expertos en campos como Diseño Industrial o de Producto, Mecánica de Fluidos, Geofísica, Ingeniería Mecánica y Ciencia de los Materiales podrían hacer valiosos aportes.
			
		\subsection{Especificación e implementación de los algoritmos y protocolos propuestos}
		
			Los algoritmos y protocolos aquí propuestos son una primera aproximación al problema; son susceptibles de ser mejorados de acuerdo con los resultados que arroje la evaluación de los mismos que aquí se hace y a la luz de un modelo de propagación de señal de radio que tome en cuenta las condiciones de operación reales de la red de sensores bajo un \textit{debris flow}, \textit{mudflow} o flujo hiperconcentrado.\\
			
			Una vez replanteados y evaluados mediante simulación, utilizando modelos más rigurosos y realistas; estos protocolos podrán ser implementados en la plataforma de \textit{hardware} y \textit{software} que se haya adoptado.

		\subsection{Desarrollo del receptor de la alerta}
		
			Debe desarrollarse también uno o varios tipos de elementos receptores de la alerta, que la reciban de la red de sensores y alerten a la población.\\
			
			En su concepción más básica, estos dispositivos recibirán la alerta de la red y activarán un dispositivo de señalización (alarma o sirena). También es posible un planteamiento más elaborado, en el que dispositivos más sofisticados que también envíen alertas dirigidas a autoridades y servicios de respuesta, a través de Internet mediante redes celulares; o bien, una red de dispositivos de alerta distribuidos en la población; en la cual, la alerta se propague una vez recibida en el primer dispositivo.\\
			
			Puesto que estos dispositivos también formarán parte de la red de sensores (condición necesaria para poder recibir de ellos la alerta) también estarán basados en la misma plataforma de \textit{hardware} y \textit{software} que ellos.\\
			
			El planteamiento de los dispositivos receptores dependerá del esquema y procedimiento para la recepción de la alerta; aspecto que se tratará más adelante.

		\subsection{Infraestructura de información}
		
			Aún cuando el objetivo fundamental de un sistema de alerta es avisar del evento a la población inmediatamente afectada, también resulta deseable llevar información sobre las alertas y otros datos de la red de sensores a consumidores potenciales de esa información: servicios de emergencia y autoridades civiles, que al ser notificados de la alerta podrán despachar la ayuda lo más pronto posible al sitio indicado; encargados de gestionar la red que deseen monitorear continuamente el estado de la misma y conocer en tiempo real su capacidad de respuesta ante eventos y estimar cuándo y dónde reemplazar o agregar nuevos nodos a la red. Para esto debe plantearse y construirse una infraestructura de información que haga posible su trasiego.\\
			
			Por infraestructura de información se entiende toda la plataforma de conectividad y \textit{software} que permitiría lograr el objetivo de hacer posible su trasiego. Se incluye dentro de ella: enlaces de comunicación, plataforma de red, servidores, sistemas de adquisición de datos, plataformas web y demás sistemas y aplicaciones que se adopten o se desarrollen para tal efecto. También se consideran aquí las interfaces de \textit{software} que permitan llevar estos datos a otros sistemas o plataformas.\\
			
			Un aspecto importante que se debe tomar en cuenta con respecto a esta plataforma de \textit{software} es que debe ser interoperable: debe intercambiar información con otros sistemas y plataformas. Para esto es necesario el uso de tecnologías abiertas y se sigan estándares relevantes a esta aplicación. Uno de ellos estrechamente relacionada para esta plataforma es la especificación \textit{Common Alerting Protocol} de OASIS \cite{oasis2005}.\\
			
			Para especificar esta plataforma, al menos en su etapa inicial, deberán determinarse cuestiones básicas que no forman parte de este trabajo, tales como ¿Cuál será la funcionalidad requerida de ella? ¿Quiénes serán sus usuarios? ¿Qué información se necesita y para qué? Para contestar esas preguntas deben definirse aspectos organizativos sobre la gestión de la red de sensores y potenciales destinatarios de su información: ¿Quién se hará cargo de la red? ¿A cuáles entidades, gubernamentales o no gubernamentales o no, se le enviará información? Estas preguntas pertenecen a la sección de problemas de tipo no tecnológico, que se expondrá a continuación.

	\section{Problemas de tipo no tecnológico}
	
		\subsection{Implantación}
		
			Poner en operación un sistema de este tipo supone varios problemas de logística que deben tomarse en cuenta, por ejemplo: la instalación de los dispositivos receptores. Sin embargo, tal vez el más importante es colocar los sensores, cada uno en su posición a lo largo del cause del río. Esta tarea puede suponer esfuerzos importantes y puede complicarse si deben colocarse sensores en zonas montañosas o silvestres de difícil acceso; por lo que debe estudiarse bien y ser ejecutado por un equipo de trabajo con las competencias necesarias.
	
		\subsection{Gestión del sistema}
		
			Una vez que se ponga a operar un sistema de detección, debe existir una entidad que se haga cargo del mismo: monitorear regularmente su estado para verificar que se encuentra operativo y listo para detectar un evento, dar el mantenimiento correspondiente a los equipos que lo requieran y reemplazar o agregar nuevos nodos a la red cuando sea necesario.\\
			
			Esta entidad puede ser local, regional o nacional; gubernamental o no gubernamental; puede ser creada específicamente para esta labor o se puede asignar a alguna organización existente; en este trabajo no se establece una preferencia por alguna de ellas; pero sí se afirma la necesidad de que estas responsabilidades sean asignadas, con el fin de que el sistema opere correctamente.\\
			
			También debe considerarse el aspecto de capacitación y entrenamiento sobre el sistema; en el caso de que las personas que integran la entidad encargada de gestionarla así lo requieran.

		\subsection{Adopción por comités de emergencias y servicios de respuesta}
		
			Un sistema de alerta temprana para \textit{debris flows} y fenómenos similares no puede tratarse como un esfuerzo aislado, ni mucho menos único, para resolver un problema de amenaza natural a poblaciones en riesgo; sino que debe formar parte de un esfuerzo integral y concertado para la gestión de los riesgos por amenazas naturales.\\
			
			Así pues, resulta necesario involucrar tanto a las autoridades nacionales y regionales encargadas de la gestión de este tipo de riesgos, como a los comités locales de atención de emergencia y a los cuerpos de respuesta a este tipo de emergencias; para integrar este sistema de detección en sus planes de mitigación de riesgos, recibir retroalimentación sobre el funcionamiento de la red y los mecanismos de alerta y que el sistema en general sea utilizado de la mejor forma para atender las emergencias detectadas por el sistema.\\
			
			Además es clave involucrar, capacitar y organizar a la población para responder a las alertas que reporte el sistema. Si la población cuenta con un comité local de emergencia, que contemple este tipo de amenazas, es imperativo hacerle partícipe del sistema e integrarlo a sus recursos, para que sea este comité el encargado de educar a la población en la atención de las alarmas que genere el sistema y defina los procedimientos de alerta y evacuación correspondiente.\\
			
			En resumen, el reto consiste en involucrar a las autoridades, servicios de respuesta y comités locales de emergencia para que adopten el sistema como suyo y lo integren a sus procedimientos de mitigación y respuesta a este tipo de emergencias y recibir retroalimentación en cuanto a los requisitos del sistema para que resulte lo más efectivo posible.

		\subsection{Vandalismo y robo de los sensores}
		
			Un riesgo que se enfrenta en un sistema como el planteado, es el vandalismo, voluntario o no, que pueda haber sobre los sensores, ya que estos están colocados sobre el río sin ningún tipo de vigilancia. Como se mencionó en un punto anterior, esto puede mitigarse diseñando los sensores de forma que se camuflen en el entorno del río (ej. como si fuera una piedra. También puede minimizarse diseñando los sensores para que no contengan materiales valiosos que los haga susceptibles de ser robados.\\
			
			Sin embargo, sobre este aspecto también caben medidas de prevención basadas en la educación y sensibilización de la población. Al estar involucrada y ser consciente de que esos sensores forman parte de un sistema que puede salvar sus vidas es esperable que la misma población se encargue de cuidarlos y prevenir su robo o vandalismo. Por lo tanto, esta educación y sensiblización también debe contemplarse para la implementación de un sistema de este tipo.
			
			\begin{figure}[htp]
				\centering
				\includegraphics[width=20cm, bb=0 0 1785 1094]{images/fig3-1-relacionProblemasImplementacionArquitectura.png}
				\\[0.2cm]
				\caption{Esquema y relación de los problemas que deben resolverse para lograr una implementación real de un sistema de detección basado en la arquitectura propuesta.}
				\label{fig:capasProtocolosRedSensores}
			\end{figure}
