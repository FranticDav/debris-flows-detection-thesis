
[301]

Bender, M. A., & Slonim, D. K. (1995). The power of team exploration: two robots can learn unlabeled directed graphs. Proceedings 35th Annual Symposium on Foundations of Computer Science, 75-85. IEEE Comput. Soc. Press. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=365703

@article{Bender_Slonim_1995, title={The power of team exploration: two robots can learn unlabeled directed graphs}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=365703}, journal={Proceedings 35th Annual Symposium on Foundations of Computer Science}, publisher={IEEE Comput. Soc. Press}, author={Bender, M A and Slonim, D K}, year={1995}, pages={75--85}}


[302]

Beerliova, Z., Eberhard, F., Erlebach, T., Hall, A., Hoffmann, M., Mihalʼak, M., & Ram, L. S. (2009). Network Discovery and Verification. (S. Albers, R. H. Möhring, G. C. Pflug, & R. Schultz, Eds.)IEEE Journal on Selected Areas in Communications, 24(12), 2168-2181. Springer. Retrieved from http://dx.doi.org/10.1109/JSAC.2006.884015

@article{Beerliova_Eberhard_Erlebach_Hall_Hoffmann_Mihalʼak_Ram_2009, title={Network Discovery and Verification}, volume={24}, url={http://dx.doi.org/10.1109/JSAC.2006.884015}, number={12}, journal={IEEE Journal on Selected Areas in Communications}, publisher={Springer}, author={Beerliova, Z and Eberhard, F and Erlebach, T and Hall, A and Hoffmann, M and Mihalʼak, M and Ram, L S}, editor={Albers, S and Möhring, R H and Pflug, G Ch and Schultz, REditors}, year={2009}, pages={2168--2181}}




[303]

Subramanian, L., Agarwal, S., Rexford, J., & Katz, R. H. (2002). Characterizing the Internet Hierarchy from Multiple Vantage Points. ProceedingsTwentyFirst Annual Joint Conference of the IEEE Computer and Communications Societies, 00(c), 618-627. Ieee. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1019307


@article{Subramanian_Agarwal_Rexford_Katz_2002, title={Characterizing the Internet Hierarchy from Multiple Vantage Points}, volume={00}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1019307}, number={c}, journal={ProceedingsTwentyFirst Annual Joint Conference of the IEEE Computer and Communications Societies}, publisher={Ieee}, author={Subramanian, Lakshminarayanan and Agarwal, Sharad and Rexford, Jennifer and Katz, Randy H}, year={2002}, pages={618--627}}



[304]

Govindan, R., & Tangmunarunkit, H. (2000). Heuristics for Internet map discovery. Proceedings IEEE INFOCOM 2000 Conference on Computer Communications Nineteenth Annual Joint Conference of the IEEE Computer and Communications Societies Cat No00CH37064, 3, 1371-1380. Ieee. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=832534

@article{Govindan_Tangmunarunkit_2000, title={Heuristics for Internet map discovery}, volume={3}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=832534}, journal={Proceedings IEEE INFOCOM 2000 Conference on Computer Communications Nineteenth Annual Joint Conference of the IEEE Computer and Communications Societies Cat No00CH37064}, publisher={Ieee}, author={Govindan, R and Tangmunarunkit, H}, year={2000}, pages={1371--1380}}




[280]

Kotz, D., Newport, C., & Elliott, C. (2003). The mistaken axioms of wireless-network research. Dartmouth College Computer Science Technical Report, TR2003-467(TR2003-467), 1-14. Retrieved from http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.147.889&rep=rep1&type=pdf

@article{Kotz_Newport_Elliott_2003, title={The mistaken axioms of wireless-network research}, volume={TR2003-467}, url={http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.147.889&rep=rep1&type=pdf}, number={TR2003-467}, journal={Dartmouth College Computer Science Technical Report}, author={Kotz, David and Newport, Calvin and Elliott, Chip}, year={2003}, pages={1--14}}


[278]
Zhou, G., He, T., Krishnamurthy, S., & Stankovic, J. A. (2004). Impact of radio irregularity on wireless sensor networks. Proceedings of the 2nd international conference on Mobile systems applications and services MobiSYS 04, 125. ACM Press. Retrieved from http://portal.acm.org/citation.cfm?doid=990064.990081

@article{Zhou_He_Krishnamurthy_Stankovic_2004, title={Impact of radio irregularity on wireless sensor networks}, url={http://portal.acm.org/citation.cfm?doid=990064.990081}, journal={Proceedings of the 2nd international conference on Mobile systems applications and services MobiSYS 04}, publisher={ACM Press}, author={Zhou, Gang and He, Tian and Krishnamurthy, Sudha and Stankovic, John A}, year={2004}, pages={125}}



[294]

Bose, P., Devroye, L., Evans, W., & Kirkpatrick, D. (2002). On the spanning ratio of Gabriel graphs and beta-skeletons. (S. Rajsbaum, Ed.)Proceedings of the Latin American Theoretical Infocomatics LATIN, 20(2), 479-493. Springer Berlin Heidelberg. Retrieved from http://dl.acm.org/citation.cfm?id=1122727.1132387

@article{Bose_Devroye_Evans_Kirkpatrick_2002, title={On the spanning ratio of Gabriel graphs and beta-skeletons}, volume={20}, url={http://dl.acm.org/citation.cfm?id=1122727.1132387}, number={2}, journal={Proceedings of the Latin American Theoretical Infocomatics LATIN}, publisher={Springer Berlin Heidelberg}, author={Bose, Prosenjit and Devroye, Luc and Evans, William and Kirkpatrick, David}, editor={Rajsbaum, SergioEditor}, year={2002}, pages={479--493}}


[297]

Narasimhan, G., & Smid, M. H. M. (2000). Approximating the Stretch Factor of Euclidean Graphs. SIAM Journal on Computing, 30(3), 978-989. SIAM

@article{Narasimhan_Smid_2000, title={Approximating the Stretch Factor of Euclidean Graphs}, volume={30}, number={3}, journal={SIAM Journal on Computing}, publisher={SIAM}, author={Narasimhan, Giri and Smid, Michiel H M}, year={2000}, pages={978--989}}


[00305]
Meguerdichian, S., Koushanfar, F., Potkonjak, M., & Srivastava, M. B. (2001). Coverage problems in wireless ad-hoc sensor networks. (F. Koushanfar, Ed.)Proceedings IEEE INFOCOM 2001 Conference on Computer Communications Twentieth Annual Joint Conference of the IEEE Computer and Communications Society Cat No01CH37213, 3(2), 1380-1387. Ieee. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=916633

@article{Meguerdichian_Koushanfar_Potkonjak_Srivastava_2001, title={Coverage problems in wireless ad-hoc sensor networks}, volume={3}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=916633}, number={2}, journal={Proceedings IEEE INFOCOM 2001 Conference on Computer Communications Twentieth Annual Joint Conference of the IEEE Computer and Communications Society Cat No01CH37213}, publisher={Ieee}, author={Meguerdichian, S and Koushanfar, F and Potkonjak, M and Srivastava, M B}, editor={Koushanfar, FEditor}, year={2001}, pages={1380--1387}}



[00306]
Megerian, S., Koushanfar, F., Potkonjak, M., & Srivastava, M. B. (2005). Worst and best-case coverage in sensor networks. IEEE Transactions on Mobile Computing. IEEE Computer Society. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1369181

@misc{Megerian_Koushanfar_Potkonjak_Srivastava_2005, title={Worst and best-case coverage in sensor networks}, volume={4}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1369181}, number={1}, journal={IEEE Transactions on Mobile Computing}, publisher={IEEE Computer Society}, author={Megerian, S and Koushanfar, F and Potkonjak, M and Srivastava, M B}, year={2005}, pages={84--92}}


[00308]

Li, X.-Y. L. X.-Y., Wan, P.-J. W. P.-J., & Frieder, O. (2003). Coverage in wireless ad hoc sensor networks. (S. C. Misra, I. Woungang, & S. Misra, Eds.)IEEE Transactions on Computers. CRC Press. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1204831

@misc{Li_Wan_Frieder_2003, title={Coverage in wireless ad hoc sensor networks}, volume={52}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1204831}, number={2}, journal={IEEE Transactions on Computers}, publisher={CRC Press}, author={Li, Xiang-Yang Li Xiang-Yang and Wan, Peng-Jun Wan Peng-Jun and Frieder, O}, editor={Misra, Subhas Chandra and Woungang, Isaac and Misra, SudipEditors}, year={2003}, pages={753--763}}


[00307]
Ghiasi, S., Srivastava, A., Yang, X., & Sarrafzadeh, M. (2002). Optimal Energy Aware Clustering in Sensor Networks. Sensors (Peterboroug, 2(7), 258-269. Citeseer. Retrieved from http://www.mdpi.com/1424-8220/2/7/258/

@article{Ghiasi_Srivastava_Yang_Sarrafzadeh_2002, title={Optimal Energy Aware Clustering in Sensor Networks}, volume={2}, url={http://www.mdpi.com/1424-8220/2/7/258/}, number={7}, journal={Sensors (Peterboroug}, publisher={Citeseer}, author={Ghiasi, Soheil and Srivastava, Ankur and Yang, Xiaojian and Sarrafzadeh, Majid}, year={2002}, pages={258--269}}



[00309]
Chen, W.-P. C. W.-P., Hou, J. C., & Sha, L. S. L. (2004). Dynamic clustering for acoustic target tracking in wireless sensor networks. IEEE Transactions on Mobile Computing. IEEE Comput. Soc. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1318595

@misc{Chen_Hou_Sha_2004, title={Dynamic clustering for acoustic target tracking in wireless sensor networks}, volume={03}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1318595}, number={3}, journal={IEEE Transactions on Mobile Computing}, publisher={IEEE Comput. Soc}, author={Chen, Wei-Peng Chen Wei-Peng and Hou, J C and Sha, Lui Sha Lui}, year={2004}, pages={258--271}}


[00310]

Ammari, H. M., & Das, S. K. (2008). Promoting Heterogeneity, Mobility, and Energy-Aware Voronoi Diagram in Wireless Sensor Networks. IEEE Transactions on Parallel and Distributed Systems. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=4459318

@misc{Ammari_Das_2008, title={Promoting Heterogeneity, Mobility, and Energy-Aware Voronoi Diagram in Wireless Sensor Networks}, volume={19}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=4459318}, number={7}, journal={IEEE Transactions on Parallel and Distributed Systems}, author={Ammari, H M and Das, S K}, year={2008}, pages={995--1008}}


[00311]
Stojmenovic, I., Ruhil, A. P., & Lobiyal, D. K. (2006). Voronoi diagram and convex hull based geocasting and routing in wireless networks. Wireless Communications and Mobile Computing, 6(2), 247-258. John Wiley and Sons. Retrieved from http://doi.wiley.com/10.1002/wcm.384

@article{Stojmenovic_Ruhil_Lobiyal_2006, title={Voronoi diagram and convex hull based geocasting and routing in wireless networks}, volume={6}, url={http://doi.wiley.com/10.1002/wcm.384}, number={2}, journal={Wireless Communications and Mobile Computing}, publisher={John Wiley and Sons}, author={Stojmenovic, Ivan and Ruhil, Anand Prakash and Lobiyal, D K}, year={2006}, pages={247--258}}


[00287]

Jeremy Schiff and Dominic Antonelli. In-Network Belief Propagation on Sensor Networks. [Obtenido de
http://www.schiffy.com/research/papers/statsense.pdf]




[00289]
Crick, C., & Pfeffer, A. (2003). Loopy Belief Propagation as a Basis for Communication in Sensor Networks. In C. Meek & U. Kjaerulff (Eds.), Proceedings of the 19th Conference on Uncertainty in AI (Vol. 18, pp. 159-166). Morgan Kaufmann Publishers. Retrieved from http://www.ncbi.nlm.nih.gov/htbin-post/Entrez/query?db=m&form=6&dopt=r&uid=0006747034

@inbook{Crick_Pfeffer_2003, title={Loopy Belief Propagation as a Basis for Communication in Sensor Networks}, volume={18}, url={http://www.ncbi.nlm.nih.gov/htbin-post/Entrez/query?db=m&form=6&dopt=r&uid=0006747034}, number={4}, booktitle={Proceedings of the 19th Conference on Uncertainty in AI}, publisher={Morgan Kaufmann Publishers}, author={Crick, Christopher and Pfeffer, Avi}, editor={Meek, Chris and Kjaerulff, UffeEditors}, year={2003}, pages={159--166}}


[00286]

Tal Anker, Danny Dolev, and Bracha Hod. 2008. Belief Propagation in Wireless Sensor Networks - A Practical Approach. In Proceedings of the Third International Conference on Wireless Algorithms, Systems, and Applications (WASA '08), Yingshu Li, Dung T. Huynh, Sajal K. Das, and Ding-Zhu Du (Eds.). Springer-Verlag, Berlin, Heidelberg, 466-479. DOI=10.1007/978-3-540-88582-5_44 http://dx.doi.org/10.1007/978-3-540-88582-5_44

﻿@incollection {springerlink:10.1007/978-3-540-88582-5_44,
   author = {Anker, Tal and Dolev, Danny and Hod, Bracha},
   affiliation = {The Hebrew University of Jerusalem Israel},
   title = {Belief Propagation in Wireless Sensor Networks - A Practical Approach},
   booktitle = {Wireless Algorithms, Systems, and Applications},
   series = {Lecture Notes in Computer Science},
   editor = {Li, Yingshu and Huynh, Dung and Das, Sajal and Du, Ding-Zhu},
   publisher = {Springer Berlin / Heidelberg},
   isbn = {978-3-540-88581-8},
   keyword = {Computer Science},
   pages = {466-479},
   volume = {5258},
   url = {http://dx.doi.org/10.1007/978-3-540-88582-5_44},
   note = {10.1007/978-3-540-88582-5_44},
   year = {2008}
}


[00291]

Hsin, C.-fan, & Liu, M. (2002). A distributed monitoring mechanism for wireless sensor networks. Proceedings of the ACM workshop on Wireless security WiSE 02, 57-66. ACM Press. Retrieved from http://portal.acm.org/citation.cfm?doid=570681.570688

@article{Hsin_Liu_2002, title={A distributed monitoring mechanism for wireless sensor networks}, url={http://portal.acm.org/citation.cfm?doid=570681.570688}, journal={Proceedings of the ACM workshop on Wireless security WiSE 02}, publisher={ACM Press}, author={Hsin, Chih-fan and Liu, Mingyan}, year={2002}, pages={57--66}}


[00034]
Akyildiz, I. F., Su, W., Sankarasubramaniam, Y., & Cayirci, E. (2002). Wireless sensor networks: a survey. Computer Networks, 38(4), 393-422. Elsevier. Retrieved from http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024

@article{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002, title={Wireless sensor networks: a survey}, volume={38}, url={http://linkinghub.elsevier.com/retrieve/pii/S1389128601003024}, number={4}, journal={Computer Networks}, publisher={Elsevier}, author={Akyildiz, I F and Su, W and Sankarasubramaniam, Y and Cayirci, E}, year={2002}, pages={393--422}}


[00285]

Farivar, R., Fazeli, M., & Miremadi, S. G. (2005). Directed Flooding: A Fault-Tolerant Routing Protocol for Wireless Sensor Networks. 2005 Systems Communications ICW05 ICHSN05 ICMCS05 SENET05, 395-399. Ieee. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1515555

@article{Farivar_Fazeli_Miremadi_2005, title={Directed Flooding: A Fault-Tolerant Routing Protocol for Wireless Sensor Networks}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=1515555}, journal={2005 Systems Communications ICW05 ICHSN05 ICMCS05 SENET05}, publisher={Ieee}, author={Farivar, R and Fazeli, M and Miremadi, S G}, year={2005}, pages={395--399}}



[00267]

Chen, A. (2001). A scalable solution to minimum cost forwarding in large sensor networks. Proceedings Tenth International Conference on Computer Communications and Networks Cat No01EX495, 00(3), 304-309. Ieee. Retrieved from http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=956276

@article{Chen_2001, title={A scalable solution to minimum cost forwarding in large sensor networks}, volume={00}, url={http://ieeexplore.ieee.org/lpdocs/epic03/wrapper.htm?arnumber=956276}, number={3}, journal={Proceedings Tenth International Conference on Computer Communications and Networks Cat No01EX495}, publisher={Ieee}, author={Chen, A}, year={2001}, pages={304--309}}







