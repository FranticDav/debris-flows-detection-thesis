\chapter{Algoritmos y protocolos para la detección de eventos}
	\label{chap:algoritmosProtocolosDeteccionEventos}
	
	\section{Introducción} \label{sec:introduccionProtocolo}
	
		En este capítulo se detallan los algoritmos y protocolos que posibilitan la detección de \textit{debris flows} y flujos hiperconcentrados, de acuerdo con el método propuesto.\\
		
		Se proponen los siguientes algoritmos y protocolos:
		\begin{enumerate}
			
			\item Protocolo de monitoreo y detección del evento. \label{item:protocolosDeteccion}
			\item Protocolo de propagación de la alerta. \label{item:protocolosPropagacion}
			\item Protocolo y algoritmo para la estimación de la posición y velocidad del fenómeno. \label{item:protocolosEstimacion}
		\end{enumerate}
		
		En caso que no se trabaje con un mapa o topología de red definida a priori, deberá ejecutarse también un protocolo de construcción del mapa de la red, a fin que cada nodo conozca la topología de la red de sensores: quiénes son sus vecinos, cuáles de ellos están en su vecindad inmediata y cuáles no; cuáles de ellos se encuentran río arriba y cuáles río abajo. Que cada nodo de la red cuente con esta información, es necesario para para que la detección distribuida de los fenómenos de interés sea posible. La propuesta de dicho algoritmo escapa al alcance de la presente tesis.\\
		
		Con el protocolo de monitoreo y detección del evento (en el ítem \ref{item:protocolosDeteccion}) la red de sensores observa el comportamtiento de la misma para, según los criterios que se expondrán a continuación, determinar si está en presencia o no de un \textit{debris flow} o flujo hiperconcentrado.\\
		
		El protocolo de propagación de la alerta (en el ítem \ref{item:protocolosPropagacion}) define cómo ésta se difunde entre los nodos de la red, una vez que se origina en un nodo de la misma.\\
		
		Por último, el protocolo y algoritmo para la estimación de la posición y velocidad del fenómeno (en el ítem \ref{item:protocolosEstimacion}), rige cómo debe transmitirse cierta información, desde nodos determinados hasta un punto central de procesamiento, donde esa información se utiliza para estimar en tiempo real, la ubicación del \textit{debris flow} y su velocidad.\\
		
		Nótese que todos los protocolos propuestos son distribuidos, mientras que el algoritmo para la estimación de la posición y velocidad del \textit{debris flow} es centralizado. En general, el enfoque con que el que se ha abordado el problema de monitoreo y detección del evento en este trabajo, desde un principio ha sido descentralizado; sin embargo, el problema de determinar la posición y la velocidad del fenómeno tiene características particulares que lo hacen suceptible de ser abordado de otra forma, como se verá más adelante.



	\section{Detección del \textit{debris flow}}
	
		En esta sección se presenta el algoritmo distribuido planteado para la detección del evento, así como el protocolo que lo implementa.\\
		
		A manera de preámbulo, se mostrarán algunas estrategias o posibles abordajes para enfrentar el problema que se consideraron para el mismo, pero que por, las razones que se ahí se mencionan, no se utilizaron.

		\subsection{Algunos abordajes considerados para enfrentar el problema}
		
			Una posible estrategia de solución que podría surgir de primera entrada para como posible solución al problema es el de detectar el arrastre de los nodos de la red por medio de la intensidad de la señal recibida (RSSI). Así, si un nodo detecta que la potencia de la señal proveniente de sus vecinos río arriba aumenta, se está en presencia de un \textit{debris flow}. Esta estrategia de solución, sin embargo, depende del supuesto de que la potencia de la señal recibida es inversamente proporcional a la distancia entre los nodos; el cual en trabajos como \cite{Zhou_He_Krishnamurthy_Stankovic_2004, Kotz_Newport_Elliott_2003} se demuestra que en condiciones reales no se cumple.\\
			
			En \cite{Kotz_Newport_Elliott_2003} también se enumeran una serie de supuestos o errores de simplificación que comúnmente se hacen con respecto a la comunicación por radio al trabajar con redes de sensores, los cuales según demuestran, no son ciertos en condiciones reales:
			
			\begin{enumerate}
				\item La tierra es plana.
				\item El área de cobertura de un nodo es circular.
				\item Todos los radios tienen un rango similar.
				\item Los enlaces son simétricos.
				\item Si un radio recibe señal de otro, la recibe perfectamente.
				\item La intensidad de la señal recibida es una simple función de la distancia.
			\end{enumerate}
			
			Queda claro que la estrategia que se siga para la detección del fenómeno, no debe suponer como ciertos ninguno de los supuestos anteriores. Esto incluye lo siguiente:
			
			\begin{enumerate}
				\item Suponer que nodos vecinos se acercan al aumentar RSSI, o se alejan al disminuirse.
				\item Suponer que todos los nodos ubicados lo suficientemente cerca para estar dentro de un ``radio de cobertura'' serán visibles al nodo, mientras que todos los que no sean visibles están fuera de ese radio.
			\end{enumerate}
			
			Como se verá más adelante, los criterios utilizados para decidir sobre la presencia del fenómeno, no dependen de estos supuestos.\\
			
			Otro posible abordaje para este problema es el de modelar a la red como un grafo, que se comprime por efecto del arrastre de los nodos provocado por el \textit{debris flow}. Así entonces, bastaría con detectar la compresión del grafo para detectar la presencia del fenómeno.\\
			
			En \cite{Bose_Devroye_Evans_Kirkpatrick_2002, Narasimhan_Smid_2000} se habla del stretch factor (``factor de estiramiento'') y spanning ratio de los grafos. Ambas razones relacionan la distancia topológica entre dos puntos vértices del grafo, con su correspondiente distancia euclidiana. En \cite{Huang03spatiotemporalmulticast} se aplica a RIS el concepto de ''\textit{network compactness}'', o compactación del grafo que describe la topología de la red. Estas relaciones podrían efectivamente, ser utilizadas para decidir si el grafo que representa la RIS se está comprimiendo y por ende, se está detectando un \textit{debris flow}.\\
			
			El obstáculo para poner en práctica este abordaje reside en la dificultad de calcular las distancias euclidianas, ya que la ubicación exacta de cada nodo en el río no es conocida (aunque existe un mapa de red, este es topológico, no geométrico); así como tampoco lo es la distancia a través de las aristas del grafo, dadas las dificultades expuestas anteriormente para estimar la distancia entre nodos a través de la potencia de la señal de radio recibida. Utilizar hardware receptor de señal de GPS tampoco resulta posible, pues además de elevar considerablemente el costo y el consumo de energía de cada sensor, este receptor requeriría de una buena recepción de señal de GPS, lo cual no se puede garantizar en medio de un \textit{debris flow}. Así entonces, este abordaje no se puede tomar como solución al problema.\\
			
			Una estructura matemática que aparenta ser promisoria para este tipo de problemas es la de los Diagramas de Voronoi.\\
			
			En un diagrama de Voronoi \cite{Aurenhammer:1991:VDS:116873.116880} un plano donde se encuentra un conjunto \(S\), de \(n\) puntos llamados sitios, es particionado en \(n\) regiones, cada una definida por un sitio, tal que cada punto dentro de esa region se encuentra más cercano (menor distacia euclídea) a un sitio \(p\) que a cualquier otro sitio de \(S\).\\

%			\begin{figure}[h!]
%				\centering
%				
%				<<FIGURA>>Agregar referencia al texto
%							
%				\caption{Diagrama de Voronoi.}
%				\label{fig:fill3}
%			\end{figure}
			
			Los diagramas de Voronoi se han utilizado en el contexto de RIS sobre todo para determinar la cobertura de una red \cite{Meguerdichian_Koushanfar_Potkonjak_Srivastava_2001, Megerian_Koushanfar_Potkonjak_Srivastava_2005, Li_Wan_Frieder_2003}, para clustering de nodos \cite{Ghiasi_Srivastava_Yang_Sarrafzadeh_2002, Chen_Hou_Sha_2004} y enrutamiento \cite{Ammari_Das_2008, Stojmenovic_Ruhil_Lobiyal_2006}.\\
			
			Podría intuirse que el uso de esta estructura geométrica podría ser de provecho para esta aplicación. Por ejemplo, se puede pensar que el río y los sensores están ubicados en un plano (o un espacio, si se hace en tres dimensiones). Los nodos de la RIS podrían ser los sitios que definien un Diagrama de Voronoi en este plano o espacio. Un \textit{debris flow} que arrastre los nodos ocasionaría un cambio en el diagrama, el cual podría ser detectado para determinar que la red está en presencia de dicho fenómeno.\\
			
			Sin embargo, al igual que en el caso de la detección por medio de la compresión del grafo, la construcción de un diagrama de Voronoi requiere que sea conocida la ubicación de cada sitio, lo cual no se puede garantizar en este planteamiento. Por lo tanto, la detección del fenómeno por medio de detección del cambio en el Diagrama de Voronoi de la red, tampoco resulta practicable en esta propuesta.\\
			
			Otro enfoque que también se consideró fue el de aplicar técnicas de ``\textit{Belief Propagation}''. En \cite{Schiff_Antonelli_2005, Crick_Pfeffer_2003} se plantea el uso de ``\textit{Belief Propagation}'', una técnica de inferencia distribuida, basada en Campos Aleatorios de Markov (Markov Random Fields) para aplicaciones de inferencia en redes de sensores inalámbricos. Sin embargo, la implementación práctica de esta técnica en redes de sensores es un problema complejo, que supone varios retos, como se indica en \cite{springerlink:10.1007/978-3-540-88582-5-44}, los cuales escapan al alcance de esta tesis.\\
			
			En \cite{Hsin_Liu_2002} se propone un mecanismo de monitoreo distribuido para detección de eventos en el área cubierta por la red. Se plantea el concepto de neighboor monitoring, en el cual, cada sensor envía sus actualizaciones solo a sus vecinos y cada sensor observa solamente a sus vecinos. También se plantea el concepto de local decision making, donde la decisión sobre declarar y reportar la existencia de un evento, se toma de forma local, entre los nodos cercanos al mismo. Como puede observarse, este planteamiento es coherente con estos principios, pues se hace monitoreo activo entre nodos vecinos: cada nodo envía actualizaciones períodicas sobre su existencia a los nodos vecinos y está atento a escuchar las de ellos. El proceso de decisión sobre la presencia del \textit{debris flow} es local, pues se toma entre un subconjunto de nodos vecinos de la red.\\
			
			En \cite{Hsin_Liu_2002} también se plantea un sistema de doble timeout: En cada nodo, hay dos contadores para cada vecino: \(C1_{i}\) y \(C2_{i}\). \(C1_{i}\) cuenta el tiempo desde que no se recibe ningun mensaje del vecino. Al vencerse, solicita al resto de vecinos reportes del nodo \(i\). Si no las recibe en un tiempo \(C2_{i}\), se declara una alerta implícita.\\
			
			Este enfoque no se siguió para esta propuesta puesto que para el caso particular de un debris flow, que es un evento másivo, que va impactando progresivamente varios nodos de la red mientras avanza; para declarar el evento, más que la pérdida de un nodo o enlace individual, es más importante el comportamiento de la red como un todo; por lo que se optó por contar el número de cambios en la topología por unidad de tiempo, sin importar de cuál nodo en particular provienen.\\
			
			Una vez presentados otros enfoques consideraros, a continuación se presentan los criterios que se adoptaron para el planteamiento del protocolo de detección.

		\subsection{Criterios utilizados para la detección del \textit{debris flow} o flujo hiperconcentrado}

			\subsubsection{Criterio I: Cambios abruptos en la topología de la red} \label{ssc:crit1}
			
				El comportamiento natural que se puede esperar en la topología de la red de sensores, una vez instalada, es que sea muy estable: un nodo casi no debería observar cambios en la conformación de sus vecinos, excepto por algún posible enlace muy debil, que puede percibirse intermitente según condiciones del entorno. Más allá de esto, los únicos cambios esperados en la topología de la red en ausencia de un evento, son los causados por eventos muy esporádicos: desaparición de sensores por agotamiento de batería o por la extracción de alguno de ellos por actividad humana.\\
				
				Considérese ahora la red, en presencia de un \textit{debris flow} o flujo hiperconcentrado. Al arrastrar los nodos ubicados río arriba y cambiar violentamente su ubicación, se provoca un fuerte disrupción en la topología de la red, en particular sobre la sección de la red correspondiente a los nodos arrastrados. Se perderán muchos enlaces existentes, mientras que nuevos enlaces podrían formarse de forma esporádica, debido a la proximidad de los nodos en la posición que los ha puesto la avalancha. Como es natural, la confiuguración de posiciones de los sensores dentro del \textit{debris flow} serán muy inestables, al igual que los enlaces, pues en medio de lodo y escombros, su comportamiento resulta impredecible. Esta variación abrupta de la topología afectará además a los sensores ubicados en la proximidad del \textit{debris flow}, pero que todavía no han sido alcanzados; pues se podrían romper los enlaces que existían entre esos sensores y los que están ahora dentro del \textit{debris flow}; además de enlaces esporádicos que surjan entre los nodos de la proximidad y otros nodos dentro del \textit{debris flow}.\\
				
				En resumen, la topología de la sección de la red arrastrada por el \textit{debris flow} y en sus proximidades, será en extremo inestable.\\
				
				Defínase la tasa de cambios en la topología local (parte de la red directamente conectada al sensor) como el número de cambios que ocurren en ella (enlaces perdidos con vecinos, o enlaces que aparecen de pronto) durante una unidad de tiempo determinada:
				
				\begin{equation}
					t.c.t. =  \frac{\text{$n^{\circ}$ cambios}}{\text{unidad de tiempo}}
					\label{equ:tasaCambiosTopologiaLocal}
				\end{equation}
				
				Esta métrica puede considerarse como una medición cuantitativa de la inestabilidad de la red, observada desde un nodo. Así pues, podría definirse un valor umbral, que por debajo del cual se contemplen las variaciones esporádicas de la topología de la red de sensores en estado estable, según se explicó anteriormente y que por encima del mismo se considere que se está en presencia de un evento que debe reportarse.\\
				
				La escogencia de este valor umbral para una red de sensores dada dependerá de la densidad de nodos y enlaces de la red, así como de la función de distribución de probabilidad en el tiempo de cambios en la topología de la red en estado estable.  Además, este umbral debe contemplar un margen de ruido por mensajes que por interferencias o colisiones (si la la red es suceptible a ellas), no lleguen a su destino.\\
				
				El uso de este criterio en un algoritmo implica una revisión periódica de los enlaces de comunicación de un nodo hacia sus vecinos; lo que puede hacerse mediante mensajes períodicos de tipo ping-pong (el nodo envía un mensaje broadcast y espera la respuesta de cada uno de sus vecinos.
				
			\subsubsection{Criterio II: Detección de mensajes directos provenientes de nodos fuera de lugar} \label{ssc:crit2}
			
				Considere una red de sensores que monitorea un río. Sea \(R\) el conjunto de todos los sensores que forman la red. Considere el nodo \(i \in R\). El nodo \(i\) cuenta con un conjunto de nodos vecinos, con los que tiene comunicación directa. A este conjunto se le llamará \(D_{i}\).\\
				
				Depediendo de su posición en la red, el nodo \(i\) tendrá un conjunto de nodos río arriba y otro río abajo. Sea \(A\) el conjunto de nodos río arriba y \(B\) el conjunto de nodos río abajo:
				\begin{equation}
					A_{i} = {a_{i1}, a_{i2}, a_{i3}, \cdots , a_{in}}
					\label{equ:fill1}
				\end{equation}
				
				\begin{equation}
					B_{i} = {b_{i1}, b_{i2}, b_{i3}, \cdots , bp_{in}}
					\label{equ:fill2}
				\end{equation}
				
				Así pues, el conjunto de vecinos directos hacia arriba será la intersección de los conjuntos \(D_i\) y \(A_i\). De igual forma, los vecinos directos hacia abajo serán la intersección de \(D_i\) y \(B_i\).
				
				\begin{equation}
					\text{Vecinos directos (río arriba): } AD_{i} = D_{i} \cap A_{i}
					\label{equ:fill3}
				\end{equation}
				
				\begin{equation}
					\text{Vecinos directos (río abajo): } BD_{i} = D_{i} \cap B_{i}
					\label{equ:fill4}
				\end{equation}
				
				Por último, puede definirse dos conjuntos de nodos ocultos, los nodos que están en la misma rama del nodo que no son visibles a éste.
				
				\begin{equation}
					\text{Nodos ocultos (río arriba): } AH_{i} = A_{i} - AD_{i}
					\label{equ:fill5}
				\end{equation}
				
				\begin{equation}
					\text{Nodos ocultos (río abajo): } BH_{i} = B_{i} - BD_{i}
					\label{equ:fill6}
				\end{equation}
				
				Una vez puesta en funcionamiento, cuando ha alcanzado su estado estable, excepto por enlaces débiles con problemas de intermitencia, por definición no deberían ser visibles desde el nodo \(i\), mensajes que provengan directamente de nodos en \(AH_{i}\) ni de \(BH_{i}\).	Así pues, el hecho que se reciban en \(i\), mensajes provenientes de estos conjuntos, puede considerarse una anomalía y por tanto, indicio de la presencia de un evento.\\
				
				La detección de mensajes de nodos provenientes de \(AH_{i}\) puede presumirse como evidencia de que se aproxima un \textit{debris flow}, el cual ha arrastrado nodos que se encontraban río arriba hasta una posición tal que sus mensajes son visibles desde el nodo \(i\). A su vez, la detección de mensajes provenientes de \(BH\) puede presumirse como evidencia de que el nodo \(i\) ha sido arrastrado por el \textit{debris flow}, y por lo tanto puede recibir mensajes de nodos ahora cercanos, que estaban anteriormente más lejos río abajo.\\
				
				No resulta conveniente apresurarse a declarar una alerta a la primera aparición de un mensaje proveniente de estos conjuntos. Podrían existir situaciones no relacionadas con un \textit{debris flow} que generen falsos positivos. Por ejemplo, que por actividad humana (incluyendo, actividad relacionada con la gestión de la red de sensores) un sensor sea movido de su ubicación inicial. También podría considerarse el caso de que un sensor no se haya colocado correctamente y haya sido arrastrado por la corriente del río. Así pues, al igual que con el criterio anterior, debe definirse un umbral de apariciones de nodos fuera de lugar, a partir del cual el nodo \(i\) declara que existe una anomalía.\\
				
				Como es evidente, para poder aplicar este criterio es necesario que cada nodo emitan periódicamente mensajes, suceptibles de ser detectados por los nodos ubicados en la proximidad, lo cual se puede cumplir con los mensajes ping-pong descritos anteriormente.
   
			\subsubsection{Criterio III: Correlación de situaciones descritas en criterios I y II, con notificaciones de esas mismas situaciones detectadas en otros nodos río arriba} 
			
				Considerar una red de sensores \(R\) en presencia de un \textit{debris flow}. ¿Cual sería el escenario más probable, observado por los distintos nodos de la red? Los nodos en la parte más arriba del río serían arrastrados de primero, por lo que serían los primeros en detectar cambios abruptos en la topología de la red. Es posible que en su camino también detecten mensajes de anuncio de nodos que anteriormente no recibían por estar fuera de su rango de alcance, río abajo. Por su parte, los nodos ubicados inmediatamente abajo de estos detectarían los cambios en la configuración de los enlaces con los nodos que están siendo arrastrados, y nodos que no tenían comunicación directa con los nodos de la parte más alta del río, podrían estar recibiendo mensajes de ellos.\\
				
				Como se puede apreciar, el comportamiento de la red en presencia de un \textit{debris flow} es observable desde distintos nodos de la red, no está localizado en un solo nodo. Esta característica permite que la detección se pueda hacer de forma distribuida y filtrar falsos positivos (anomalías observadas por uno o dos sensores).\\
				
				Bajo esta premisa, conviene que cada nodo envíe un aviso a sus vecino (y que esos propaguen el aviso) cuando observan alguna situación de atención, caracterizada por los criterios I y II. De esta forma, un nodo podría confirmar una situación observada por él, al recibir un aviso de la misma situación proveniente de otro nodo. Aun sin haber observado por sí mismo ninguna situación anómala, un nodo podría inferir que algo sucede, luego de recibir mensajes de alerta de puntos distintos de la red.\\
				
				Así pues, aparte del aviso que cada nodo envía a sus vecinos de algo podría estar ocurriendo en la red, cada nodo podría concluir, a partir de conocimiento local y avisos de otros nodos, que efectivamente, la red se encuentra en presencia de un fenómeno que debe ser reportado. A esta conclusión se le denominará ``Alerta''. Una vez generada, el nodo enviará la alerta a todos sus vecinos, quienes a su vez la propagarán, según el protocolo que se verá más adelante.\\
				
				Sea \(x_{i}\) un nodo de la red de sensores \(R\) y sea \(n_{i}\) el número de sensores que el nodo \(x_{i}\) tiene conocimiento de que han emitido algun aviso de situación anómala (excluyendo el mismo nodo \(x_{i}\)).\\
				
				\begin{equation}
					aviso_{i} = \left\{
						\begin{array}{l l}
							1 & \quad \text{si el nodo $x_{i}$ ha observado alguna situación anómala segun criterios I y II}\\
							0 & \quad \text{si no}\\
						\end{array} \right.
					\label{equ:fill7}
				\end{equation}
				
				El nodo \(x_i\) generará una \textbf{alerta} si y solo si:
				
				\begin{equation}
					n_{i} + a * aviso_{i} \ge N_{i}
					\label{equ:fill8}
				\end{equation}
					
				Donde \(a\) es el \textbf{coeficiente}, el peso que se le dará a la observación local frente a los avisos de los vecinos. Al hacer \(a = 1\), se le da el mismo peso (credibilidad) a la observación local que a los avisos provenientes de nodos vecinos. También donde \(N_{i}\) es el \textbf{valor umbral}, que es equivalente al número de nodos que tienen que haber observado una situación anómala para declarar una alerta en el nodo \(x_{i}\).\\
				
				El valor de \(N_{i}\) resulta clave, pues define la sensibilidad de la red de sensores. Un valor muy bajo produciría una red muy sensible, potencialmente suceptible a falsos positivos. Por otro lado, la escogencia de un valor excesivo de \(N_{i}\) puede provocar que la red ignore algunos eventos reales.\\
				
				Tomando en cuenta estos criterios, se planteó un protocolo para el monitoreo en la red de sensores, que permitiría la detección del fenómeno.  

		\subsection{Protocolo de monitoreo y detección de \textit{debris flows}}
		
		Al arrancar, los nodos inician un período de "calentamiento", en el que intercambian mensajes, para descubrir cuales son los vecinos de cada nodo.Luego, la red ingresa en régimen permanente o estacionario. Durante este modo, los nodos ejectuan el siguiente protocolo de monitoreo:
		
		\begin{enumerate}
		
			\item Periódicamente, cada nodo emite a sus vecinos un mensaje, en el cual anuncia que está presente y activo en la red. Esto ocurre en lo que se denomina una ventana, que es una franja de tiempo de duración \(T_{v}\), que ocurre cada período \(T\). Durante esta ventana, los nodos esperan un tiempo aleatorio para enviar ese anuncio y escuchar y contestar mensajes. Una vez finalizado \(T_{v}\), los nodos pasan a un período de descanso \(T_{d} = T - T_{v}\).
			
			
			\item Cada nodo hace un conteo de el número de enlaces que desaparecieron, más los que aparecieron, desde la ventana anterior. Si esa cuenta excede un valor umbral previamente definido, se activará una bandera interna. Esta bandera se desactivará si trascurren \(M\) períodos, no necesariamente consecutivos, en los que el número de enlaces cambiados sea menor a \(N_{umb}\).
	
			\begin{enumerate}
				\item Si la bandera permanece activa por \(k\) períodos consecutivos, el nodo emviará a sus vecinos un mensaje de AVISO, de forma inmediata si la plataforma lo permite, o en la próxima ventana, si no. El mensaje de aviso incluye el ID del nodo que la originó, estampa de tiempo y causa de transmisión (en este caso, \(t.c.c\) excedida). También incluye un parámetro \(TTL\) (\textit{Time To Live}) que se decrementa cada vez que es reenviado. Al llegar a \(0\), el \textbf{aviso} no se reenvía más.
				
				\item Cada nodo que reciba este mensaje de aviso, lo reenviará también a sus vecinos, decrementando el \(TTL\). El mesaje no se vuelve a reenviar si ya había llegado a ese nodo anteriormente.
				
				\item Una vez generado el aviso, o al haber llegado desde otro nodo, este se mantiene vigente en el nodo por un timpo definido. (\(T\) aviso) Una vez transcurrido este \(T\) aviso, el mismo es descartado.

			\end{enumerate}
			
			\item Detección de nodos fuera de lugar: Por cada ventana, se hace un conteo de mensajes provenientes de los conjuntos \(AH_{i}\) y \(BH_i\). Si el conteo combinado de las últimas \(N\) ventanas consecutivas conteo excede el umbral predefinido \(C_{fl}\); se envía un mensaje de aviso, igual a como se indica en los pasos anteriores, con causa de transmisión ``\(C_{fl}\) excedido''.
			
			\item Implementación del criterio III: Cada nodo hace un conteo de las banderas que se han declarado internamente en el nodo (\(aviso_{i}\)); así como también de las que han llegado de nodos externos (\(n_{i}\)) y determina si se cumple la siguiente inecuación:
			\begin{equation}
				n_{i} + a * aviso_{i} \ge N_{i}
				\label{equ:fill10}
			\end{equation}
			
			Donde \(a\) y \(N_{i}\) son constantes definidas en el criterio III.\\
			
			En caso de cumplirse, el nodo declara \textbf{alerta} y ejecuta el protocolo correspondiente, que se detallará más adelante.
			
			\item Una vez terminada la ventana, el nodo se pone en reposo durante otro período \(T_{d}\) y se continúa indefinidamente con el algoritmo.

		\end{enumerate}
		
		Todos los mensajes de anuncio, así como todas sus respuestas, deben ser enviadas antes que termine la ventana \(T_{v}\). Una vez que esta se complete, los nodos resposarán hasta que inicie un nuevo período \(T\) y se repitan los pasos descritos.\\
		
		Cada nodo puede agrupar o hacer \textit{piggyback} de varios mensajes, incluyendo respuestas a varios nodos o su anuncio, si todavía no lo ha hecho durante esa ventana de tiempo.\\
		
		El período \(T\), debe estar dentro de un rango tal, que permita a la red encontrar cambios en la topología, dentro de un tiempo de reacción aceptable; pero que también evite el agotamiento temprano de la batería debido al uso excesivo del radio. Adicionalmente, para lograr un mayor tiempo de vida de las baterías, si la plataforma lo permite, puede implementarse un esquema de transmisión en que los sensores permanecen dormidos la mayor parte del tiempo, excepto en la ventana \(T_{d}\). Esto implica que los sensores deban tener sincronizados sus relojes.\\
		
			
		En un principio cada nodo se encuentra en modo construcción mapa, donde se sigue el protocolo descrito en la sección anterior. Una vez construido, la red pasa al modo estable, en el que conmuta continuamente entre los estados (dormido) y Ventana, con un período \(T\). Al detectarse que se cumple el criterio I o II, o recibir un aviso de otro nodo, se pasa al estado aviso, en el que se pasa al estado alerta si se confirma la emergencia; o se regresa al modo normal si esto no sucede.

	\section{Propagación de la alerta}
	
		Una vez declarada la alerta en un nodo de la red, esta debe propagarse a través de la red hasta los centros de población que deben ser alertados, ubicados aguas abajo en el río.\\
	
		El protocolo de propagación de la alerta debe ser de alta confiabilidad, para asegurarse que esta llegue a la población requerida. Además, debe ser de respuesta rápida, para que  llegue tan pronto como sea posible. En estas circunstancias, otros aspectos como optimizar el enrutamiento para minimizar el consumo de energía o alargar la vida de la red pierden relevancia, sobre todo en esta aplicación, en la que el evento tiene como efecto la destrucción de la misma red de sensores.\\
	
		En esta propuesta, para la propagación de la alerta, se ha escogido la técnica de flooding o inundación de paquetes  \cite{Akyildiz_Su_Sankarasubramaniam_Cayirci_2002}. Esto debido a que es una forma efectiva de propagar el estado de alerta, a tantos nodos de la red, tan rápido como sea posible.
	
		A continuación, se hace una descripción detallada del protocolo planteado.
	
		\subsection{Protocolo de propagación de la alerta:}
	
			\begin{enumerate}
			
				\item El nodo que declaró la alerta envía un broadcast de alerta a sus vecinos directos, tan pronto como sea posible. El mensaje de alerta incluye el ID del nodo que la originó. Este broadcast de alerta se vuelve a transmitir a intervalos regulares (en cada período ventana, si los otros nodos no son capaces de recibir mensajes durante el tiempo de descanso \(T_{d}\))
				
				\item \label{item:step2prop} Cada vecino que recibe la alerta, la reenvía a sus vecinos; excepto si la alerta se originó o ya había pasado por el nodo, (O sea, solo se reenvía una vez, no cada vez que llegue.) en cuyo caso se limita a seguirla retransmitiendo como se indicó en el paso 1. Luego la seguirá retransmitiendo por períodos regulares.
				
				\item La alerta llega a los nodos ubicados en la parte baja del río, donde están ubicados los receptores. Estos al recibir la alerta, activarán el mecanismo de notificación a la población.
				
				\item Una vez que se declara o se recibe una alerta, se desactiva el protocolo de monitoreo y detección. No se emiten ni se reenvían más avisos en la red de sensores.
				
			\end{enumerate}
			
			Nótese que en el paso \ref{item:step2prop}, la alerta debe ser reenviada por todos los nodos que la reciban, incluyendo los que se encuentren río arriba y los que se encuentren también en otras ramas. Esto podría verse como excesivo e inecesario, pues lo requrido en primera instancia es que la alerta se propague río abajo, hasta llegar a las poblaciones afectadas. Sin embargo, debe tomarse en cuenta que el mapa de red construido al inicio, es una linealización de un tejido más complejo: no se puede garantizar que la topología plasmada en el mapa sea un refejo fiel de la realidad. Algunos pares de nodos que aparecen con un orden dado en el mapa, podrían tener ese orden invertido en la realidad. También debe tomarse en cuenta que podría darse el caso que un nodo \(A\), que se encuentra entre un par de nodos \(B\) (ubicado arriba de \(A\)) y \(C\) (ubicado río abajo de \(A\)) no tenga comunicación directa con \(C\) pero sí la tiene con \(B\), que además la tiene con \(C\). Al transmitir el mensaje a \(B\), \(B\) puede retransmitirlo a \(C\), cosa que \(A\) no podía realizar por sí solo.\\
			
			Por lo anterior, se infiere que el protocolo será más robusto si se inunda toda la red con mensajes de alerta, que si solo retransmitieran los nodos que se encuentran río abajo del nodo que declara o retransmite la alerta (con lo que se tendría algo similar a un flooding dirigido \cite{Farivar_Fazeli_Miremadi_2005} o MCPF \cite{Chen_2001} ); por lo que se plantea así en esta propuesta. 

	\section{Estimación de posición y velocidad del \textit{debris flow}} 
	
		En la sección \ref{sec:introduccionProtocolo} se justificó la adopción de un enfoque centralizado para la determinación de la posición y velocidad del \textit{debris flow}, argumentando que este problema tenía características particulares que lo hacen propicio a este paradigma. Entre ellas, se pueden mencionar las siguientes:
		
		\begin{enumerate}
			\item La estimación requiere acceso a datos geoespaciales o georeferenciados, los cuales no son prácticos de tener en la red de sensores y exigen mayor capacidad de procesamiento, lo cual excede las prestaciones de los nodos sensores.
			\item No es tan crítico como la detección del fenómeno, propiamente dicha, lo cual elimina la presión por hacer un planteamiento distribuido de alta robustez, similar al que se planteó para la detección.
		\end{enumerate}
		
		\subsection{Protocolo de notificación, de aproximación y llegada del fenómeno a los nodos}
		
			Una vez declarada o recibida la alerta, cada nodo alertado realiza lo siguiente (al tiempo que vuelve a trasmitir la alerta a intervalos regulares):
			
			\begin{enumerate}
				\item El nodo permanece a la escucha de mensajes entrantes. 
				\item Cuando detecte mensajes que provengan directamente de algun nodo del conjunto \(AH_{i}\) o \(BH_{i}\) (cualquier tipo de mensaje: anuncio, aviso, alerta) realizará lo siguiente:
				\begin{enumerate}
					\item Mientras se haya recibido solo mensajes provenientes de \(AH_{i}\), se enviará un mensaje de información (llamado \textbf{tipo I}) río abajo, indicando dicha situación. Incluirá también: ID del nodo, estampa de tiempo. La retransimisión de este mensaje podrá realizarse a intervalos regulares y combinarse (piggybacking) con la ALERTA y otros mensajes retransmitidos.
					\item En el momento que se reciba algun mensaje proveniente directamente de alguin nodo en \(BH_{i}\), al igual que en el paso anterior, se enviará un mensaje que indica indica que se han recibido mensajes de \(BH_{i}\). A estos mensajes se les denomina del \textbf{tipo II}. Este mensaje se retransmitirá a intervalos regulares. En lo sucesivo no se envían más mensajes que indiquen presencia de nodos de \(AH_{i}\) (tipo I).
				\end{enumerate}
				\item Cada nodo que reciba un mensaje del tipo I deberá reenviarlo a sus vecinos, siempre que el mensaje provenga de un nodo río arriba de la misma rama (si proviene de un nodo río abajo, no deberá reenviarlo). Los mensajes del tipo II deben reenviarse siempre que sean de la misma rama, a menos que ya hayan pasado por el nodo.
				\item Estos mensajes, tipo I y tipo II llegarán a los nodos inferiores, donde al menos uno de ellos estará enlazado con un centro de procesamiento, que analizará los datos y estimará en tiempo real la posición y velocidad del fenómeno.
			\end{enumerate}
			
%			\begin{figure}[h!]
%				\centering
%				
%				<<FIGURAS>>
%							
%				\caption{El nodo con nodos superiores cerca emitiendo, y el nodo cerca de nodos inferiores.}
%				\label{fig:fill4}
%			\end{figure}
			
			Según se vio en el capítulo \ref{chap:arquitecturaSistemaDeteccionPropuesto}, el instante \(t\) en que el \textit{debris flow} arrastra al nodo \(i\) se puede acotar entre instantes \(t_{1}\) y \(t_{2}\), donde \(t_{1}\) es el último instante donde se reciben solo mensajes de nodos anteriormente ocultos río arriba y \(t_{2}\) el primer instante donde se reciben mensajes ubicados río abajo. Nótese que eso es lo que se hace en este protocolo, enviar al nodo raíz primero la notificación desde en nodo \(i\) de que se están recibiendo mensajes de nodos superiores (el fenómeno se aproxima) y luego que se están recibiendo mensajes de nodos río abajo (el nodo ha sido arrastrado).\\
			
			Es importante mencionar que este protocolo se basa en el supuesto que la transmisión inalámbrica desde nodos ubicados dentro del \textit{debris flow} hacia nodos ubicados fuera de él es posible. El correcto funcionamiento de esta técnica está sujeta al cumplimiento de este supuesto.
			
		\subsection{Algoritmo de estimación de la posición y velocidad}
		
			A diferencia de los algoritmos presentados anterioremente, este algoritmo no es distribuido, sino que será ejecutado por un elemento de procesamiento (supernodo, computadora personal, servidor, dispositivo embebido) acoplado al nodo raíz o un algún otro nodo ubicado en la parte inferior del río.\\
			
			Este elemento debe tener conocimiento de la geografía del río. Idealmente también debería tener conocimiento de la ubicación geoespacial de cada nodo sensor. Si esto no es posible, podría tenerse la ubicación exacta de un pequeño subconjunto de los nodos de la red y aproximarse la del resto.\\
			
			El algoritmo no brinda una ubicación exacta o aproximada del \textit{debris flow}, más bien define un rango en el cual se puede acotar su presencia.\\
			
%			\begin{figure}[h!]
%				\centering
%				
%				<<FIGURA>>
%							
%				\caption{Cotas superior e inferior.}
%				\label{fig:fill5}
%			\end{figure}
%			
%			El algoritmo es el siguiente:
%			
%			%% RECOMIENDO USAR EL PAQUETE ALGORITHMIC SI ES UN ALGORITMO EN CODIGO --- CHRISTIAN


			\begin{enumerate}
			
				\item Una vez llegue la alerta al elemento de procesamiento, se espera a que comiencen a llegar las notificaciones tipo I y tipo II.
				
				\item De todas las notificaciones tipo I que vayan llegando, se selecciona de entre las que tienen la estampa de tiempo más reciente, la que provenga del nodo en la posición más superior. La ubicación del sensor que emitió esa notificación define la cota inferior, o punto más abajo en el río  donde podría estar el \textit{debris flow}.\\
				
				Es decir, el primer criterio es la estampa de tienpo, el segundo la posición.
				
				\item De todas las notifiaciones tipo II que vayan llegando, se selecciona la que se haya originado en el punto más aguas abajo del río. La ubicación del sensor que la haya generado define la cota superior, o punto más río arriba donde podría estar el \textit{debris flow}.\\
				
				La posición del \textit{debris flow} estará limitada entre las cotas superior e inferior. Así pues, podría darse un dato aproximado de la posición del \textit{debris flow}:
				
				\begin{equation}
					x_{aprox} = \frac{( x_{sup} + x_{inf} )}{2}
					\label{equ:fill11}
				\end{equation}
				
				La incertidumbre de esa aproximación es: 
				
				\begin{equation}
					err_{aprox} = | x_{sup} - x_{inf} | 
					\label{equ:fill12}
				\end{equation}
				
				\item Cuando la cota superior de la posición del fenómeno se mueva entre un punto y otro, se calculará la diferencia entre las estampas de tiempo de la posición actual y la anterior. La velocidad del \textit{debris flow} estará dada por:
				
				\begin{equation}
					v  = \frac{\Delta d}{\Delta t}
					\label{equ:fill13}
				\end{equation}
				
				Esto es donde \(\Delta d\) es la distancia entre la posición actual de la cota superior y la anterior y \(\Delta t\) es la diferencia de las estampas de tiempo de la posición actual y la anterior.\\
				
				Si la distancia \(\Delta d\) está dada en metros y \(t\) está dada en segundos, la velocidad estará dada en \(^{m}/_{s}\). Multiplicando esta velocidad por el factor \textbf{3.6}, se puede obtener la velocidad en \(^{km}/_{h}\).

			\end{enumerate}
